package Arrays;

public class Matrix {

    public static void main(String[] args) {

        int[][] matrix = {
                {2, 4, 6},
                {8, 4, 2},
                {3, 6, 9}
        };

        int[][] b = new int[3][3];

        int[][] a = new int[3][3];


        for (int i = 0; i < matrix.length; i++) {
            for (int j = matrix[i].length - 1; j >= 0; j--) {
                b[i][2 - j] = matrix[i][j];
            }
            System.out.println();
        }

        for (int i = 0; i < b.length; i++) {
            for (int j = 0; j < b[i].length; j++) {
                a[i][j] = b[j][i];
            }
            System.out.println();
        }


        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }


    }
}
