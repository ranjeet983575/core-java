package Arrays;


public class Search {

    public static int linearSearch(int[] array, int value) {
        if (array == null || array.length == 0) {
            System.err.println("Empty Array Found");
            return -1;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                return i;
            }
        }
        return -1;
    }

    public static int binarySearch(int[] array, int value) {
        int low = 0;
        int high = array.length - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (array[mid] == value) {
                return mid;
            } else if (value > array[mid]) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 6, 0, -1, 5};
        int[] array1 = {1, 2, 3, 4, 5};
        System.out.println(linearSearch(array, 3));
        System.out.println(linearSearch(array, 30));

        System.out.println(binarySearch(array, 3));
        System.out.println(binarySearch(array, 30));

    }
}
