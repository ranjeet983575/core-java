package Arrays;

import java.util.Arrays;

public class PrintPairWithGivenSum {

    public static void main(String[] args) {
        int array[] = {1, 4, 5, 6, 8, 3, 2, 0};
        int sum = 9;
        printUsingBrutForce(array, sum);
        print(array, sum);

    }

    public static void printUsingBrutForce(int[] array, int sum) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                int sum1 = array[i] + array[j];
                if (sum == sum1) {
                    System.out.println(array[i] + "****" + array[j]);
                }
            }
        }

    }

    public static void print(int[] array, int sum) {
        int low = 0;
        int high = array.length - 1;
        Arrays.sort(array);
        while (low < high) {
            int sum1 = array[low] + array[high];
            if (sum1 == sum) {
                System.out.println(array[low] + "--" + array[high]);
                low++;
                high--;
            } else if (sum1 > sum) {
                high--;
            } else {
                low++;
            }

        }


    }
}
