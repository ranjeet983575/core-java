package Arrays;

public class ArrayADT {

    private int[] array;
    private int index;
    private int size;

    public ArrayADT(int size) {
        this.size = size;
        this.index = 0;
        this.array = new int[size];
    }

    public void printArray() {
        System.out.println();
        for (int i = 0; i < index; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    // OperationMethods
    public void insert(int data) {
        if (index == size) {
            System.out.println("Array is Full");
            return;
        }
        array[index++] = data;
    }

    public void delete(int deleteIndex) {
        if (deleteIndex < 0 || deleteIndex >= index) {
            System.out.println("Invalid Index");
            return;
        }
        for (int i = deleteIndex; i < index - 1; i++) {
            array[i] = array[i + 1];
        }
        index--;
        array[index] = 0;
    }

    public int search(int data) {
        for (int i = 0; i < index; i++) {
            if (array[i] == data) {
                return array[i];
            }
        }
        return -1;
    }

    public int getIndex(int data) {
        for (int i = 0; i < index; i++) {
            if (array[i] == data) {
                return i;
            }
        }
        return -1;
    }

    public int get(int index) {
        if (index < 0 || index > this.index) {
            System.out.println("Invalid index");
            return -1;
        }
        return array[index];
    }

    public void update(int index, int data) {
        if (index < 0 || index > this.index) {
            System.out.println("Invalid index");
            return;
        }
        array[index] = data;
    }

    public int getSize() {
        return this.index;
    }


    public static void main(String[] args) {
        ArrayADT array = new ArrayADT(5);
        array.insert(1);
        array.insert(2);
        array.insert(3);
        array.insert(4);
        array.insert(5);
        array.printArray();
        array.insert(6);
        array.delete(3);
        array.printArray();
        System.out.println("Size : " + array.getSize());
//        System.out.println(array.search(2));
//        System.out.println(array.search(20));
//        System.out.println(array.getIndex(2));
//        System.out.println(array.getIndex(20));
//        System.out.println(array.get(3));
//        System.out.println(array.get(10));
        array.update(3, 30);
        array.printArray();
        System.out.println("Size : " + array.getSize());
    }

}
