package Arrays;

public class FindMaxSubArraySum {
    public static void main(String[] args) {
        int[] array = {1, 2, -5, 4, 3, 8, 5};
        int[] array1 = {-2, -1};
        System.out.println(findSum(array));
        System.out.println(findSum(array1));
    }

    public static int findSum(int[] array) {
        int sum = array[0];
        int maxSum = array[0];
        for (int i = 1; i < array.length; i++) {
            if (sum < 0) {
                sum = array[i];
            } else {
                sum = sum + array[i];
            }
            if (sum > maxSum) {
                maxSum = sum;
            }
//            maxSum = Math.max(sum, maxSum);
        }
        return maxSum;
    }
}
