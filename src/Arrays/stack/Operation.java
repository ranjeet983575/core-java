package Arrays.stack;

import java.util.Stack;

public class Operation {
    public static void main(String[] args) {
        System.out.println(reverseString("HELLO"));
        System.out.println("Is Balanced : " + isBalanceParentheses("[{()}]"));
        System.out.println("Is Balanced : " + isBalanceParentheses("[{())]"));
        System.out.println("Is Balanced : " + isBalanceParentheses("[{("));
    }

    public static String reverseString(String str) {
        StringBuilder sb = new StringBuilder();
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            stack.push(str.charAt(i));
        }
        while (!stack.isEmpty()) {
            Character ch = stack.pop();
            sb.append(ch);
        }
        return sb.toString();
    }

    public static boolean isBalanceParentheses(String expression) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < expression.length(); i++) {
            char ch = expression.charAt(i);
            if (ch == '(' || ch == '{' || ch == '[') {
                stack.push(ch);
            } else if (ch == ')' || ch == '}' || ch == ']') {
                if (stack.isEmpty()) {
                    return false;
                }
                Character pop = stack.pop();
                if (!isMatchingPair(pop, ch)) {
                    return false;
                }

            }
        }
        return stack.isEmpty();
    }


    public static boolean isMatchingPair(char open, char close) {
        if (open == '(' && close == ')') {
            return true;
        } else if (open == '{' && close == '}') {
            return true;
        } else return open == '[' && close == ']';
    }

    public String evaluatePostFix(String str) {

        return null;
    }
}
