package Arrays.stack;

public class Stack {

    int[] array;
    int maxSize;
    int top;

    public Stack(int size) {
        this.maxSize = size;
        this.array = new int[maxSize];
        this.top = -1;
    }

    public void push(int data) {
        if (top == maxSize - 1) {
            System.out.println("Stack is Full");
            return;
        }
        top++;
        array[top] = data;

    }

    public void display() {
        if (top == -1) {
            System.out.println("Stack is Empty");
            return;
        }
        for (int i = 0; i <= top; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public int pop() {
        if (top == -1) {
            System.out.println("Stack is Empty");
            return -1;
        } else {
            top--;
            return array[top + 1];
        }
    }

    public int peek() {
        if (top == -1) {
            System.out.println("Stack is Empty");
            return -1;
        } else {
            return array[top];
        }
    }
}
