package Arrays.queue;

public class SimpleQueueTest {

    public static void main(String[] args) {

        SimpleQueue queue = new SimpleQueue(5);
        queue.enqueue(200);
        queue.enqueue(300);
        queue.enqueue(400);
        queue.display();
        queue.enqueue(50);
        queue.display();
        queue.dequeue();
        queue.display();
        queue.enqueue(500);
        queue.enqueue(600);
        queue.display();
    }
}
