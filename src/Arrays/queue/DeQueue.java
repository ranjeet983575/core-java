package Arrays.queue;

public class DeQueue {

    private int capacity;
    private int size;
    private int[] queue;
    private int front;
    private int rear;

    public DeQueue(int capacity) {
        this.capacity = capacity;
        queue = new int[capacity];
        size = 0;
        front = -1;
        rear = -1;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean isFull() {
        return size == capacity;
    }

    public boolean enqueueFront(int data) {
        if (isFull()) {
            System.out.println("Queue is Full ");
        }
        if (isEmpty()) {
            front = 0;
            rear = 0;
        } else if (front == 0) {
            front = capacity - 1;
        } else {
            front--;
        }
        queue[front] = data;
        size++;
        return true;
    }

    public boolean enqueueRear(int data) {
        if (isFull()) {
            System.out.println("Queue is Full ");
        }
        if (isEmpty()) {
            front = 0;
            rear = 0;
        } else if (rear == capacity - 1) {
            rear = 0;
        } else {
            rear++;
        }
        queue[rear] = data;
        size++;
        return true;
    }

    public int dequeueFront() {
        if (isEmpty()) {
            System.out.println("Queue is Empty");
            return -1;
        }
        int data = queue[front];
        if (front == rear) {
            front = -1;
            rear = -1;
        } else if (front == capacity - 1) {
            front = 0;
        } else {
            front++;
        }
        size--;
        return data;
    }

    public int dequeueRear() {
        if (isEmpty()) {
            System.out.println("Queue is Empty");
            return -1;
        }
        int data = queue[rear];
        if (front == rear) {
            front = -1;
            rear = -1;
        } else if (rear == 0) {
            rear = capacity - 1;
        } else {
            rear--;
        }
        size--;
        return data;
    }

    public int peekFront() {
        if (isEmpty()) {
            System.out.println("Queue is Empty");
            return -1;
        }
        return queue[front];
    }


    public int peekRear() {
        if (isEmpty()) {
            System.out.println("Queue is Empty");
            return -1;
        }
        return queue[rear];
    }

    public void display() {
        if (isEmpty()) {
            System.out.println("Queue is Empty");
            return;
        }
        for (int i = 0; i < size; i++) {
            System.out.print(queue[(front + i) % capacity] + "\t");

        }
        System.out.println();
    }

}
