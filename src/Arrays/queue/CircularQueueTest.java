package Arrays.queue;

public class CircularQueueTest {
    public static void main(String[] args) {

        CircularQueue queue = new CircularQueue(5);
        System.out.println(queue.enqueue(200));
        queue.enqueue(300);
        queue.enqueue(400);
        queue.enqueue(50);
        queue.enqueue(500);
        queue.display();
        System.out.println("Size :" + queue.getSize());
        System.out.println(queue.dequeue());
        queue.display();
        System.out.println("Size :" + queue.getSize());
        queue.enqueue(100);
        queue.display();

    }
}
