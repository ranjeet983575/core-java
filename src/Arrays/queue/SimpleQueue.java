package Arrays.queue;

public class SimpleQueue {
    private int[] queue;
    private int front;
    private int rear;
    private int capacity;

    public SimpleQueue(int capacity) {
        this.capacity = capacity;
        this.queue = new int[capacity];
        this.front = 0;
        this.rear = -1;
    }

    public void enqueue(int data) {
        if (rear == this.capacity - 1) {
            System.out.println("Queue is full");
            return;
        }
        this.rear++;
        queue[rear] = data;
    }

    public int dequeue() {
        if (front > rear) {
            System.out.println("Queue is Empty");
            return -1;
        }
        int data = queue[front];
        this.front++;
        return data;
    }

    public int peek() {
        if (front > rear) {
            System.out.println("Queue is Empty");
            return -1;
        }
        return queue[front];
    }

    public void display() {
        if (front > rear) {
            System.out.println("Queue is Empty");
            return;
        }
        for (int i = front; i <= rear; i++) {
            System.out.print(queue[i] + "\t");
        }
        System.out.println();
    }
}
