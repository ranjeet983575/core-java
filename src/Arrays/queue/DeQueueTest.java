package Arrays.queue;

public class DeQueueTest {
    public static void main(String[] args) {

        DeQueue queue = new DeQueue(5);
        System.out.println(queue.enqueueFront(20));
        queue.enqueueFront(10);
        queue.enqueueRear(30);
        queue.display();
        queue.enqueueFront(2000);
        queue.enqueueRear(3000);
        queue.display();
        queue.dequeueFront();
        queue.display();
        queue.dequeueRear();
        queue.display();

    }
}
