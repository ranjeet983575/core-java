package Arrays.queue;

public class CircularQueue {

    private int[] queue;
    private int rear;
    private int front;
    private int size;
    private int capacity;

    // Constructor to initialize the queue
    public CircularQueue(int capacity) {
        this.capacity = capacity;
        this.queue = new int[capacity];
        this.front = 0;
        this.rear = 0;
        this.size = 0;
    }

    // Enqueue method to add an element
    public boolean enqueue(int data) {
        if (isFull()) {
            System.out.println("Queue is full!");
            return false; // Indicate failure
        }
        queue[rear] = data;
        rear = (rear + 1) % capacity; // Update rear pointer circularly
        size++;
        return true; // Indicate success
    }

    // Dequeue method to remove an element
    public int dequeue() {
        if (isEmpty()) {
            System.out.println("Queue is empty!");
            return -1;
        }
        int data = queue[front];
        front = (front + 1) % capacity; // Update front pointer circularly
        size--;
        return data;
    }

    // Peek method to view the front element
    public int peek() {
        if (isEmpty()) {
            System.out.println("Queue is empty!");
            return -1;
        }
        return queue[front];
    }

    // Get the current size of the queue
    public int getSize() {
        return size;
    }

    // Display the elements of the queue
    public void display() {
        if (size == 0) {
            System.out.println("Queue is empty!");
            return;
        }
        System.out.print("Queue elements: ");
        for (int i = 0; i < size; i++) {
            int index = (front + i) % capacity;
            System.out.print(queue[index] + " ");
        }
        System.out.println();
    }

    // Check if the queue is empty
    public boolean isEmpty() {
        return size == 0;
    }

    // Check if the queue is full
    public boolean isFull() {
        return size == capacity;
    }
}
