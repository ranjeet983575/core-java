package Arrays;

import java.util.*;
import java.util.stream.Collectors;

public class MapDemo {

    public static void main(String[] args) {
        Map<String,Integer> map=new HashMap();
        map.put("A",1);
        map.put("C",3);
        map.put("D",4);
        map.put("B",2);

//        Map<String, Integer> result = map.entrySet().stream().sorted(Map.Entry.comparingByValue())
//                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue,(integer, integer2) -> integer,HashMap::new));
//
//        System.out.println(result);

        List<Map.Entry<String, Integer>> collect = map.entrySet().stream().sorted(Map.Entry.comparingByValue()).collect(Collectors.toList());

        System.out.println(collect.size());
        collect.stream().forEach(r -> {
            System.out.println(r.getKey()+" "+r.getValue());
        });

    }
}
