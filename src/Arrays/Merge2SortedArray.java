package Arrays;

public class Merge2SortedArray {
    public static void main(String[] args) {

        int[] arr1={1,3,4};
        int[] arr2={2,4,5,6};
        int[] result=new int[arr1.length+arr2.length];

        mergeArray(arr1,arr2,result);
    }

    public static void mergeArray(int[] arr1,int[]arr2,int[] result)
    {

        int i=0,j=0,k=0;

        while (i<arr1.length && j<arr2.length) {

            if (arr1[i] < arr2[j]) {
                result[k++] = arr1[i++];
            } else {
                result[k++] = arr2[j++];
            }
        }

        while (i<arr1.length)
        {
            result[k++]=arr1[i++];
        }
        while (j<arr2.length)
        {
            result[k++]=arr2[j++];
        }

        for (int a=0;a<result.length;a++)
        {
            System.out.print(result[a]+" ");
        }

    }


}








