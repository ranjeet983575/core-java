package Arrays;

import java.util.*;

public class SortOccurence {

    public static void main(String[] args) {

        Integer[] array1 = {5,2,8,8,5,5,8,1,9,0,1,1,0,1};
        List<Integer> list = Arrays.asList(array1);
        Set temp = new HashSet(list);
        List<Data> data=new ArrayList<>();

        temp.forEach(o -> {
            Data d1=new Data((int)o, Collections.frequency(list, o));
            data.add(d1);
        });
        data.stream().sorted(Comparator.comparing(Data::getValue)).forEach(data1 -> {
            for (int i=0;i<data1.getValue();i++)
            {
                System.out.print(data1.getKey()+" ");
            }
        });

    }

}

class  Data{
    private int key;
    private int value;

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Data(int key, int value) {
        this.key = key;
        this.value = value;
    }
}

