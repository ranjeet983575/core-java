package Arrays;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class DuplicateElement {

    public static void main(String[] args) {
        int array[] = {1, 4, 5, 6, 8, 5, 3, 2, 0, 8};
        findDuplicateUnsorted(array);
        findDuplicateSorted(array);
    }

    public static void findDuplicateUnsorted(int[] array) {
        Set<Integer> uniqueNumber = new HashSet<>();
        boolean isDuplicate = false;

        for (int i = 0; i < array.length; i++) {
            int num = array[i];
            if (uniqueNumber.contains(num)) {
                isDuplicate = true;
                System.out.println("Duplicate Numbers is : " + num);
            } else {
                uniqueNumber.add(num);
            }
        }

        if (!isDuplicate) {
            System.out.println("Duplicate Not Found");
        }
    }

    public static void findDuplicateSorted(int[] array) {
        Arrays.sort(array);

        int len=array.length;
        int j = 0;

        for (int i = 0; i < len - 1; i++) {
            if (array[i] != array[i + 1]) {
                array[j++] = array[i];
            }
        }
        array[j++] = array[len - 1];

        for (int i = 0; i < j; i++) {
            System.out.print(array[i]+" ");
        }

    }

}
