package Arrays;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ArraySum {
    public static void main(String[] args) {
        int[] array = {2, 7, 3, 6, 1, 8, 5, 7, 8, 9};
        int sum = 9;
        System.out.println(findSum(array, sum));
    }

    public static Set<String> findSum(int[] array, int sum) {
        Set<String> result = new HashSet<>();
        Map<Integer, Boolean> temp = new HashMap<>();
        for (int num : array) {
            int complement = sum - num;
            if (temp.containsKey(complement)) {
                int smaller = Math.min(num, complement);
                int larger = Math.max(num, complement);
                result.add(smaller + " - " + larger);
            }
            temp.put(num, true);
        }
        return result;
    }
}
