package Arrays;

/*
 * Beginner approach
 * Time complexity
 * 0(n*3)
 * */
public class CommonElement {

    public static void main(String[] args) {

        int[] array1 = {1, 5, 10, 20, 40, 80};
        int[] array2 = {6, 7, 20, 80, 100};
        int[] array3 = {3, 4, 15, 20, 30, 70, 80, 120};
        commonNormalApproach(array1, array2, array3);
        commonElements(array1, array2, array3);
    }

    public static void commonNormalApproach(int[] array1, int[] array2, int[] array3) {
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                for (int k = 0; k < array3.length; k++) {
                    if (array1[i] == array2[j] && array2[j] == array3[k]) {

                        System.out.println(array1[i]);

                    }
                }
            }
        }
    }

    public static void  commonElements(int arr1[], int arr2[], int arr3[]) {

        //Declare three variables and initialized to zero
        int x = 0, y = 0, z = 0;

        while(x < arr1.length && y < arr2.length && z < arr3.length) {

            if(arr1[x] == arr2[y] && arr2[y] == arr3[z]) {
                System.out.println(arr1[x]);
                x++;
                y++;
                z++;
            } else if (arr1[x] > arr2[y]) {
                y++;

            } else if (arr2[y] > arr3[z]) {
                z++;

            } else {
                x++;
            }
        }
    }


}
