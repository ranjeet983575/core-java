package Numbers;

import java.util.Random;

public class Numbers {
    public static void main(String[] args) {

        System.out.println(reverseNumber(123));
        System.out.println("Is Prime : " + isPrime(2));
        System.out.println("Is Prime : " + isPrime(17));
        System.out.println("Is Prime : " + isPrime(9));

        System.out.println("Is Palindrome :" + isPalindrome(222));
        System.out.println("Is Palindrome :" + isPalindrome(2123));

        System.out.println("Factorial of 5 : " + factorial(5));
        System.out.println("Factorial of 6 : " + factorial(6));

        fibonacci(5);

        System.out.println("Single Digit sum of 12345 : " + findSingleDigitSum(12345));
        System.out.println("Random Number between 10 to 50 is : " + findRandomNumber(10, 50));


    }

    public static int reverseNumber(int num) {
        int sum = 0;
        while (num != 0) {
            int rem = num % 10;
            sum = sum * 10 + rem;
            num = num / 10;
        }
        return sum;
    }

    public static boolean isPalindrome(int num) {
        int rev = reverseNumber(num);
        return rev == num;
    }

    public static int findSingleDigitSum(int num) {
        int sum = 0;
        while (num != 0 || sum > 9) {
            if (num == 0) {
                num = sum;
                sum = 0;
            }
            sum = sum + (num % 10);
            num = num / 10;
        }
        return sum;
    }

    public static int findRandomNumber(int start, int end) {
        
        return (int) (Math.random() * (end - start + 1)) + start;

    }

    public static boolean isPrime(int num) {
        boolean flag = true;
        if (num <= 1) {
            return false;
        }
        if (num == 2) {
            return true;
        }
        for (int i = 2; i < num / 2; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static long factorial(int number) {
        long result = 1;
        for (int i = 2; i <= number; i++) {
            result = result * i;
        }
        return result;
    }

    public static void fibonacci(int terms) {
        int a = 0;
        int b = 1;
        System.out.print(a + " " + b + " ");
        for (int i = 2; i < terms; i++) {
            int c = a + b;
            a = b;
            b = c;
            System.out.print(c + " ");
        }

    }

}
