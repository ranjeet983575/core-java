package DS;

public class InsertionSort {

    public static int[] sort(int[] array) {

        for (int i = 0; i < array.length; i++) {
            int temp = array[i];
            int j = i - 1;
            while (j >= 0 && array[j] > temp) {
                array[j + 1] = array[j];
                j = j - 1;
            }
            array[j + 1] = temp;
        }
        return array;
    }

    public static void main(String[] args) {
        int[] array = {5, 2, 3, 6, 9, 8, 5};
        int[] sort = InsertionSort.sort(array);
        for (int i = 0; i < sort.length; i++) {
            System.out.print(sort[i] + " ");
        }
    }


}
