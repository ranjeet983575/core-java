package DS;

class QueueWithArray {
    int front, rear, size;
    int capacity;
    int array[];

    private QueueWithArray(int capacity) {
        this.capacity = capacity;
        front = this.size = 0;
        rear = capacity - 1;
        array = new int[this.capacity];
    }

    private boolean isFull(QueueWithArray queue) {
        return (queue.size == queue.capacity);
    }

    private boolean isEmpty(QueueWithArray queue) {
        return (queue.size == 0);
    }

    // adding element to push
    private void push(int item) {
        if (isFull(this)) {
            return;
        } else {
            this.rear = (this.rear + 1) % this.capacity;
            this.array[this.rear] = item;
            this.size = this.size + 1;
            System.out.println(item + " Added");
        }
    }

    private int pop() {
        if (isEmpty(this)) {
            return Integer.MIN_VALUE;
        } else {
            int item = this.array[this.front];
            this.front = (this.front + 1) % this.capacity;
            this.size = this.size - 1;
            return item;
        }
    }

    private int front() {
        if (isEmpty(this)) {
            return Integer.MIN_VALUE;
        } else {
            return this.array[this.front];
        }
    }

    private int rear() {
        if (isEmpty(this)) {
            return Integer.MIN_VALUE;
        } else {
            return this.array[this.rear];
        }
    }

    public static void main(String[] args) {
        QueueWithArray queue = new QueueWithArray(10);
        queue.push(10);
        queue.push(20);
        queue.push(30);
        queue.push(40);
        queue.push(50);

        queue.pop();
        queue.pop();

        System.out.println("Front Element : " + queue.front());
        System.out.println("Last Element : " + queue.rear());

        System.out.println("-----------------");
        for (int i = 0; i < queue.array.length; i++) {
            if (queue.array[i] != 0) {
                System.out.print(queue.array[i]);
                System.out.print(",");
            }
        }
    }

}
