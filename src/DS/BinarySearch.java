package DS;

import java.util.Arrays;

public class BinarySearch {

    public static int search(int[] array, int key) {
        int low = 0;
        int high = array.length - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (array[mid] == key) {
                return mid;
            } else if (key <= array[mid]) {
                high = mid - 1;

            } else {
                low = mid + 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8};

        System.out.println(BinarySearch.search(array, 2));
        System.out.println(BinarySearch.search(array, 9));
    }

}
