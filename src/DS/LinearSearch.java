package DS;

public class LinearSearch {

    public static int search(int[] array, int key) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] array = {5, 2, 3, 6, 9, 8, 5};
        System.out.println(LinearSearch.search(array, 2));
        System.out.println(LinearSearch.search(array, 1));
    }

}
