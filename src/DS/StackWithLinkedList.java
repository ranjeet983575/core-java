package DS;

import java.util.EmptyStackException;
import java.util.List;

public class StackWithLinkedList {

    class ListNode {
        private int data;
        private ListNode next;

        public ListNode(int data) {
            this.data = data;
        }
    }

    private ListNode top;
    private int length;

    public StackWithLinkedList() {
        top = null;
        length = 0;
    }

    public int length() {
        return length;
    }

    public boolean isEmpty() {
        return length == 0;
    }

    public void push(int data) {
        ListNode newNode = new ListNode(data);
        newNode.next = top;
        top = newNode;
        length++;
    }

    public int  pop() {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        int data = top.data;
        top = top.next;
        length--;
        return data;
    }

    public int peek() {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        return top.data;
    }

    public void printAll() {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        ListNode current = top;
        System.out.println();
        while (current != null) {
            System.out.print(current.data + "-->");
            current = current.next;
        }
        System.out.println("null");
    }


    public static void main(String[] args) {
        StackWithLinkedList stack = new StackWithLinkedList();
        stack.push(5);
        stack.push(10);
        stack.push(15);
        stack.push(20);
        stack.push(25);
        stack.push(30);

        System.out.println(stack.peek());
		System.out.println(stack.pop());
        System.out.println(stack.peek());

        stack.printAll();

    }


}
