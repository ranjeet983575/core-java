package DS;

class StackWithArray {
    static final int MAX = 100;
    int top;
    int array[] = new int[MAX];

    boolean isEmpty() {
        return top < 0;
    }

    StackWithArray() {
        top = -1;
    }

    public boolean push(int x) {
        if (top >= (MAX - 1)) {
            System.err.println("Stack Overflow");
            return false;
        } else {
            array[++top] = x;
            System.out.println(x + " Pushed into stack");
            return true;
        }
    }

    public int pop() {
        if (top < 0) {
            System.err.println("Stack UnderFlow");
            return 0;
        } else {
            int x = array[top--];
            return x;
        }
    }

    public static void main(String[] args) {
        StackWithArray stack = new StackWithArray();
        stack.push(10);
        stack.push(20);
        stack.push(30);
        stack.push(40);
        stack.push(50);
        System.out.println(stack.pop() + " has Poped Fron Stack");
        System.out.println(stack.pop() + " has Poped Fron Stack");
        System.out.println(stack.pop() + " has Poped Fron Stack");
    }
}
