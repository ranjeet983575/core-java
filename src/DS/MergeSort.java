package DS;

public class MergeSort {

    public static int[] sort(int[] array1, int[] array2) {
        int len1 = array1.length;
        int len2 = array2.length;
        int[] result = new int[len1 + len2];
        int i = 0, j = 0, k = 0;

        while (i < len1 && j < len2) {
            if (array1[i] < array2[j]) {
                result[k] = array1[i];
                i++;
            } else {
                result[k] = array2[j];
                j++;
            }
            k++;
        }

        while (i < len1) {
            result[k] = array1[i];
            i++;
            k++;
        }
        while (j < len2) {
            result[k] = array2[j];
            j++;
            k++;
        }
        return result;
    }

    public static void main(String[] args) {
        int[] array1 = {2, 3, 6, 8, 9};
        int[] array2 = {4, 5, 8, 15};
        int[] sort = MergeSort.sort(array1, array2);

        for (int i = 0; i < sort.length; i++) {
            System.out.print(sort[i] + " ");
        }
    }


}
