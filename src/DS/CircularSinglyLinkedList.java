package DS;

public class CircularSinglyLinkedList {

    private ListNode last;
    private int length;

    private class ListNode {
        private int data;
        private ListNode next;

        public ListNode(int data) {
            this.data = data;
        }
    }

    public CircularSinglyLinkedList() {
        last = null;
        length = 0;
    }

    public int length() {
        return length;
    }

    public void insertAtBeginning(int data) {
        ListNode newNode = new ListNode(data);
        if (last == null) {
            last = newNode;
            last.next = last;
        } else {
            newNode.next = last.next;
        }
        last.next = newNode;
        length++;
    }

    public void insertAtEnd(int data) {
        ListNode newNode = new ListNode(data);
        if (last == null) {
            last = newNode;
            last.next = last;
        } else {
            newNode.next = last.next;
            last.next = newNode;
            last = newNode;
        }
        length++;
    }

    public void printList() {
        if (last == null) {
            return;
        }

        ListNode current = last.next;
        while (current != last) {
            System.out.print(current.data + "-->");
            current = current.next;
        }
        System.out.println(current.data);
    }

    public void removeFirst() {
        if (isEmpty()) {
            return;
        }
        ListNode temp = last.next;
        if (last.next == last) {
            last = null;
            return;
        }
        last.next = temp.next;
        temp.next = null;
        length--;
    }

    public void removeLast(){
        if (isEmpty()) {
            return;
        }
        ListNode current = last.next;
        ListNode temp = last.next;
        if (last.next == last) {
            last = null;
            return;
        }
        while(current.next!=last)
        {
            current=current.next;
        }
        last=current;
        last.next=temp;
        length--;
    }

    public boolean isEmpty() {
        return length == 0; //last==null;
    }

    public static void main(String[] args) {
        CircularSinglyLinkedList list = new CircularSinglyLinkedList();
        list.insertAtBeginning(50);
        list.insertAtBeginning(40);
        list.insertAtBeginning(30);
        list.insertAtBeginning(20);
        list.insertAtBeginning(10);
        list.insertAtEnd(60);
        list.printList();
        list.removeFirst();
        list.printList();
        list.removeLast();
        list.printList();
    }


}
