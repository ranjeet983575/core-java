package DS;

public class ArrayDemo {

    public void moveZeroToEnd() {
        int[] arr = {0, 8, 1, 0, 2, 1, 0, 3, 0};
        int j = 0;
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

        for (int i = 0; i < arr.length; i++) {
            for (j = i; j < arr.length - 1; j++) {
                if (arr[j] == 0 && arr[j + 1] != 0) {
                    int temp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = temp;

                }
            }
        }

        System.out.println();
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }


    public int[] resizeArray(int arr[],int capacity)
    {
        int[] temp=new int[capacity];
        for (int i = 0; i < arr.length; i++) {
            temp[i]=arr[i];
        }
        arr=temp;
        return arr;
    }


    public static void main(String[] args) {

        ArrayDemo demo = new ArrayDemo();
        demo.moveZeroToEnd();
        int[] arr = {0, 8, 1, 0, 2, 1, 0, 3, 0};
        System.out.println();
        System.out.println(arr.length);
        int[] arr1 = demo.resizeArray(arr, arr.length * 2);
        System.out.println(arr1.length);

    }
}
