package DS;



public class SinglyLinkedList {

    class Node {
        int data;
        Node next;

        Node(int data) {
            this.data = data;
        }
    }


    Node head = null;
    // adding element into beginning of  list
    public void addFirst(int new_data) {
        Node newNode = new Node(new_data);
        if (head == null) {
            head = newNode;
            return;
        }
        newNode.next = head;
        head = newNode;
    }

    // adding element into end of  list
    public void addLast(int new_data) {
        Node newNode = new Node(new_data);
        if (head == null) {
            head = newNode;
            return;
        }
        Node currentNode = head;
        while (currentNode.next != null) {
            currentNode = currentNode.next;
        }
        currentNode.next = newNode;
    }

    public void addAt(int value, int position) {
        Node newNode = new Node(value);
        if (position == 1) {
            newNode.next = head;
            head = newNode;
            return;
        }
        Node previous = head;
        int count = 1;
        while (count < position - 1) {
            previous = previous.next;
            count++;
        }
        Node current = previous.next;
        newNode.next = current;
        previous.next = newNode;
    }

    public void addSortedList(int value)
    {
           Node newNode=new Node(value);
           Node current =head;
           Node temp=null;
           while(current!=null && current.data<newNode.data)
           {
               temp=current;
               current=current.next;

           }
           newNode.next=current;
           temp.next=newNode;
    }

    public void printList() {
        Node node = head;
        while (node != null) {
            System.out.print(node.data + "-->");
            node = node.next;
        }
        System.out.println("null");
    }

    public int findLength() {
        if (head == null) {
            return 0;
        }
        int count = 0;
        Node currentNode = head;
        while (currentNode != null) {
            count++;
            currentNode = currentNode.next;
        }
        return count;
    }

    public void deleteFirstNode() {
        if (head != null) {
            Node temp = head;
            head = head.next;
            temp.next = null;
        }
    }

    public void deleteLastNode() {
        if (head != null && head.next != null) {
            Node current = head;
            Node previous = null;
            while (current.next != null) {
                previous = current;
                current = current.next;
            }
            previous.next = null;
        }
    }

    public void deleteAt(int position) {
        if (position == 1) {
            head = null;
        } else {
            int count = 1;
            Node previous = head;
            while (count < position - 1) {
                previous = previous.next;
                count++;
            }
            Node current = previous.next;
            previous.next = current.next;
        }
    }

    public void deleteByValue(int value)
    {
        Node temp=null;
        Node current=head;

        while(current!=null && current.data!=value)
        {
            temp=current;
            current=current.next;
        }
        if(current==null)
        {
            return;
        }else{
            temp.next=current.next;
        }
    }

    public boolean search(int value) {
        if (head != null) {
            Node current = head;
            while (current != null) {
                if (current.data == value) {
                    return true;
                }
                current = current.next;
            }
        }
        return false;
    }

    public void reverseList() {
        if (head != null) {
            Node current = head;
            Node previous = null;
            Node next = null;
            while (current != null) {
                next = current.next;
                current.next = previous;
                previous = current;
                current = next;
            }
            head = previous;
        }
    }


    public void findMiddleNode() {
        Node pointerX1 = head;
        Node pointerX2 = head;

        while (pointerX1 != null && pointerX2.next != null) {
            pointerX1 = pointerX1.next;
            pointerX2 = pointerX2.next.next;
        }
        System.out.println("Middle Node is =" + pointerX1.data);
    }

    public void findNthPosFromEndOfList(int nthPos) {
        Node mainPointer = head;
        Node refPointer = head;
        int count = 0;
        while (count < nthPos) {
            refPointer = refPointer.next;
            count++;
        }
        while (refPointer!=null)
        {
            refPointer=refPointer.next;
            mainPointer=mainPointer.next;
        }
        System.out.println("The Data Of position "+nthPos+" is "+mainPointer.data);
    }

    public void removeDuplicateList(){
        Node current=head;
        while(current!=null && current.next!=null)
        {
            if(current.data==current.next.data)
            {
                current.next=current.next.next;
            }else
            {
                current=current.next;
            }
        }
    }




    public static void main(String[] args) {
        SinglyLinkedList list = new SinglyLinkedList();
        list.addFirst(10);
        list.addFirst(20);
        list.addFirst(30);
        list.addFirst(40);
        list.addFirst(50);
        list.printList();
        list.addLast(5);
        list.printList();
        list.addAt(60, 1);
        list.addAt(35, 4);
        list.printList();
        list.deleteFirstNode();
        list.printList();
        list.deleteLastNode();
        list.printList();
        list.deleteAt(4);
        list.printList();
        System.out.println(list.search(20));
        System.out.println(list.search(21));
        list.reverseList();
        list.printList();
        list.findMiddleNode();
        list.findNthPosFromEndOfList(4);
        list.printList();
        list.addLast(50);
        list.printList();
        list.removeDuplicateList();
        list.printList();
        list.addSortedList(45);
        list.printList();
        list.deleteByValue(45);
        list.printList();
        System.out.println("Length of List is = " + list.findLength());


    }
}
