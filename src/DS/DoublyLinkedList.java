package DS;

public class DoublyLinkedList {

    private ListNode head;
    private ListNode tail;
    private int length;

    private class ListNode {
        int data;
        ListNode prev;
        ListNode next;

        public ListNode(int data) {
            this.data = data;
        }
    }

    public DoublyLinkedList() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    public boolean isEmpty() {
        return length == 0; //head==null;
    }

    public int length() {
        return length;
    }

    public void displayForward() {
        if (head == null) {
            return;
        }

        ListNode temp = head;
        System.out.println();
        while (temp != null) {
            System.out.print(temp.data + " -> ");
            temp = temp.next;
        }
        System.out.print("null");
    }

    public void displayBackward() {
        if (tail == null) {
            return;
        }
        ListNode temp = tail;
        System.out.println();
        while (temp != null) {
            System.out.print(temp.data + " -> ");
            temp = temp.prev;
        }
        System.out.print("null");
    }


    public void insertAtBeginning(int data) {
        ListNode newNode = new ListNode(data);
        if (isEmpty()) {
            tail = newNode;
        } else {
            head.prev = newNode;
        }
        newNode.next = head;
        head = newNode;
        length++;
    }

    public void insertAtEnd(int data) {
        ListNode newNode = new ListNode(data);
        if (isEmpty()) {
            head = newNode;
        } else {
            tail.next = newNode;
        }
        newNode.prev = tail;
        tail = newNode;
        length++;
    }

    public void insertAt(int position, int data) {
        if (length < position) {
            return;
        }
        if (position == 1) {
            insertAtBeginning(data);
            return;
        }
        ListNode newNode = new ListNode(data);
        int count = 1;
        ListNode current = head;
        ListNode temp = null;
        while (count < position - 1) {
            current = current.next;
            count++;
        }
        temp = current.next;
        current.next = newNode;
        newNode.prev = current;
        newNode.next = temp;
        length++;
    }

    public void removeFirstNode() {
        if (isEmpty()) {
            return;
        }
        ListNode temp = head;
        if (head == tail) {
            tail = null;
        } else {
            head.next.prev = null;
        }
        head = head.next;
        temp.next = null;
        length--;
    }

    public void removeLastNode() {
        if (isEmpty()) {
            return;
        }
        ListNode temp = tail;
        if (head == tail) {
            head = null;
        } else {
            tail.prev.next = null;
        }
        tail = tail.prev;
        temp.prev = null;
        length--;
    }

    public void removeAt(int position) {
        if (length < position) {
            return;
        }
        if (position == 1) {
            removeFirstNode();
            return;
        }
        int count = 1;
        ListNode current = head;
        ListNode temp = null;
        while (count < position - 1) {
            current = current.next;
            count++;
        }
        temp = current.next;
        current.next=temp.next;
        temp.prev=current;
        length--;
    }



    public static void main(String[] args) {
        DoublyLinkedList list = new DoublyLinkedList();
        list.insertAtEnd(5);
        list.insertAtEnd(10);
        list.insertAtEnd(15);
        list.insertAtEnd(20);
        list.insertAtEnd(25);
        list.insertAtEnd(30);
        list.displayForward();
        list.displayBackward();
        list.removeFirstNode();
        list.displayForward();
        list.removeLastNode();
        list.displayForward();
        list.insertAt(3, 18);
        list.displayForward();
        list.removeAt(3);
        list.displayForward();



    }

}
