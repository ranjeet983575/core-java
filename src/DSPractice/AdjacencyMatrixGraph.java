package DSPractice;

public class AdjacencyMatrixGraph {

    private int V;
    private int E;
    private int[][] adjMatrix;

    public AdjacencyMatrixGraph(int nodes) {
        this.V = nodes;
        this.E = 0;
        this.adjMatrix = new int[nodes][nodes];
    }

    public void addEdge(int u, int v) {
        this.adjMatrix[u][v] = 1;
        this.adjMatrix[v][u] = 1;
        E++;
    }

    public void printMatrix() {
        for (int i = 0; i < adjMatrix[0].length; i++) {
            for (int j = 0; j < adjMatrix.length; j++) {
                System.out.print(adjMatrix[i][j] + "  ");
            }
            System.out.println();
        }
        System.out.println("Number Of Edges is : " + E);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(V + " vertices, " + E + " edges" + "\n");
        for (int v = 0; v < V; v++) {
            sb.append(v + ": ");
            for (int w : adjMatrix[v]) {
                sb.append(w + " ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        AdjacencyMatrixGraph g = new AdjacencyMatrixGraph(4);
        g.addEdge(0, 1);
        g.addEdge(1, 2);
        g.addEdge(2, 3);
        g.addEdge(3, 0);

        System.out.println(g.toString());
    }
}
