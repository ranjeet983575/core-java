package DSPractice;


import java.util.List;

class SinglyLinkedList {
    ListNode head;

    static class ListNode {
        public int data;
        private ListNode next = null;

        public ListNode(int data) {
            this.data = data;
            this.next = null;
        }
    }

    public void display() {
        ListNode current = head;
        while (current != null) {
            System.out.print(current.data + " --> ");
            current = current.next;
        }
        System.out.print("null");
        System.out.println();
    }

    public int length() {
        int count = 0;
        if (head == null) {
            return count;
        }
        ListNode current = head;
        while (current != null) {
            count++;
            current = current.next;
        }
        return count;
    }

    public void insetFirst(int data) {
        ListNode newNode = new ListNode(data);
        if (head == null) {
            head = newNode;
            return;
        }
        newNode.next = head;
        head = newNode;
    }

    public void insetLast(int data) {
        ListNode newNode = new ListNode(data);
        if (head == null) {
            head = newNode;
            return;
        }
        ListNode current = head;
        while (null != current.next) {
            current = current.next;
        }
        current.next = newNode;
    }

    public void insertAt(int data, int position) {
        if (position == 0 || position > length() + 1) {
            System.out.println("Invalid Position");
            return;
        }
        ListNode newNode = new ListNode(data);
        if (position == 1) {
            newNode.next = head;
            head = newNode;
        } else {
            ListNode previous = head;
            int count = 1;
            while (count < position - 1) {
                previous = previous.next;
                count++;
            }
            ListNode current = previous.next;
            newNode.next = current;
            previous.next = newNode;
        }
    }

    public ListNode deleteFirst() {
        if (head == null) {
            return null;
        }
        ListNode temp = head;
        head = head.next;
        temp.next = null;
        return temp;
    }

    public ListNode deleteLastNode() {
        if (head == null || head.next == null) {
            return null;
        }
        ListNode current = head;
        ListNode previous = null;
        while (current.next != null) {
            previous = current;
            current = current.next;
        }
        previous.next = null;
        return current;
    }

    public ListNode deleteAt(int position) {
        if (head == null) {
            return null;
        }
        if (position == 1) {
            ListNode temp = head;
            head = head.next;
            temp.next = null;
            return temp;
        } else {
            ListNode previous = head;
            int count = 1;
            while (count < position - 1) {
                previous = previous.next;
                count++;
            }
            ListNode current = previous.next;
            previous.next = current.next;
            return current;

        }
    }

    public boolean search(int data) {
        if (head == null) {
            System.out.println("List is Empty");
            return false;
        } else {
            ListNode current = head;
            int count = 1;
            while (current.next != null) {
                if (current.data == data) {
                    System.out.println("Data Found At Position : " + count);
                    return true;
                }
                count++;
                current = current.next;
            }
        }
        return false;
    }

    public ListNode findMiddleNode() {
        if (head == null) {
            return null;
        }
        ListNode slowPtr = head;
        ListNode fastPtr = head;
        while (fastPtr != null && fastPtr.next != null) {
            slowPtr = slowPtr.next;
            fastPtr = fastPtr.next.next;
        }
        return slowPtr;
    }

    public ListNode findNthNode(int n) {
        if (head == null)
            return null;

        ListNode mainPtr = head;
        ListNode refPtr = head;
        int count = 1;
        while (count <= n) {
            count++;
            refPtr = refPtr.next;
        }
        while (refPtr != null) {
            refPtr = refPtr.next;
            mainPtr = mainPtr.next;
        }
        return mainPtr;
    }

    public void removeDuplicateShortedList() {
        if (head == null)
            return;
        ListNode current = head;
        while (current != null && current.next != null) {
            if (current.data == current.next.data) {
                current.next = current.next.next;
            } else {
                current = current.next;
            }
        }
    }

    public void insertSortedList(int data) {
        if (head == null) {
            return;
        }
        ListNode newNode = new ListNode(data);
        ListNode current = head;
        ListNode temp = null;
        while (current != null && current.data < data) {
            temp = current;
            current = current.next;
        }
        newNode.next = current;
        temp.next = newNode;
    }

    public void removeByValue(int value) {
        if (head == null) {
            return;
        }
        ListNode current = head;
        ListNode temp = null;
        while (current != null && current.data != value) {
            temp = current;
            current = current.next;
        }
        temp.next = current.next;
    }

    public void createLoopLinkedList() {
        ListNode first = new ListNode(1);
        ListNode second = new ListNode(2);
        ListNode third = new ListNode(3);
        ListNode fourth = new ListNode(4);
        ListNode fifth = new ListNode(5);
        ListNode six = new ListNode(6);

        head = first;
        first.next = second;
        second.next = third;
        third.next = fourth;
        fourth.next = fifth;
        fifth.next = six;
        six.next = third;
    }

    public boolean findLoopInLinkedList() {
        if (head == null) {
            return false;
        }
        ListNode slowPtr = head;
        ListNode fastPtr = head;
        while (fastPtr != null && fastPtr.next != null) {
            slowPtr = slowPtr.next;
            fastPtr = fastPtr.next.next;
            if (slowPtr == fastPtr) {
                return true;
            }
        }
        return false;
    }

    public ListNode findStartLoopInLinkedList() {
        if (head == null) {
            return null;
        }
        ListNode slowPrt = head;
        ListNode fastPtr = head;
        while (fastPtr != null && fastPtr.next != null) {
            slowPrt = slowPrt.next;
            fastPtr = fastPtr.next.next;
            if (slowPrt == fastPtr) {
                return findStart(slowPrt);
            }
        }
        return null;
    }

    private ListNode findStart(ListNode slowPrt) {
        ListNode temp = head;
        while (slowPrt != temp) {
            temp = temp.next;
            slowPrt = slowPrt.next;
        }
        return temp;
    }

    public void removeLoopInLinedList() {
        if (head == null) {
            return;
        }
        ListNode fastPtr = head;
        ListNode slowPtr = head;
        while (fastPtr != null && fastPtr.next != null) {
            fastPtr = fastPtr.next.next;
            slowPtr = slowPtr.next;
            if (slowPtr == fastPtr) {
                removeLoop(slowPtr);
                return;
            }
        }
    }

    private void removeLoop(ListNode slowPtr) {
        ListNode temp = head;
        while (slowPtr.next != temp.next) {
            temp = temp.next;
            slowPtr = slowPtr.next;
        }
        slowPtr.next = null;
    }

    public ListNode mergeSortedList(ListNode a, ListNode b) {
        ListNode dummy = new ListNode(0);
        ListNode tail = dummy;
        while (a != null && b != null) {
            if (a.data <= b.data) {
                tail.next = a;
                a = a.next;
            } else {
                tail.next = b;
                b = b.next;
            }
            tail = tail.next;
        }
        if (a == null) {
            tail.next = b;
        } else {
            tail.next = a;
        }

        return dummy.next;
    }


}

public class SinglyLinkedListExample {

    public static void main(String[] args) {

        SinglyLinkedList list = new SinglyLinkedList();
        System.out.println("Length of List is : " + list.length());
        list.insetLast(3);
        list.insetLast(3);
        //list.insetLast(4);
        list.insetLast(7);
        list.insetLast(8);
        list.display();
        System.out.println("Length of List is : " + list.length());

        list.insertAt(5, 3);
        list.insertAt(6, 4);
        list.insertAt(2, 1);

        list.display();

        SinglyLinkedList list1 = new SinglyLinkedList();
        list1.insetLast(1);
        list1.insetLast(4);
        list1.insetLast(8);

        SinglyLinkedList list2 = new SinglyLinkedList();
        list2.insetLast(2);
        list2.insetLast(6);


//
//        System.out.println("Deleted First Node : " + list.deleteFirst().data);
//
//        list.display();
//
//        System.out.println("Deleted Last Node : " + list.deleteLastNode().data);
//        list.display();
//
//        System.out.println("Deleted At Position 3 is : " + list.deleteAt(3).data);
//        list.display();
//
//        list.search(6);

//        System.out.println("Middle Pointer Element is : " + list.findMiddleNode().data);
//        System.out.println("Nth Pointer Element is : " + list.findNthNode(4).data);
//        list.removeDuplicateShortedList();
//        list.display();
//        list.insertSortedList(4);
//        list.display();
//        list.removeByValue(8);
//        list.display();

        list.createLoopLinkedList();
        System.out.println("Is Loop Exist In LinkedIn : " + list.findLoopInLinkedList());
        System.out.println("Start Of Loop Is : " + list.findStartLoopInLinkedList().data);
        list.removeLoopInLinedList();
        System.out.println("Is Loop Exist In LinkedIn : " + list.findLoopInLinkedList());
        list.display();

        list1.display();
        list2.display();

        SinglyLinkedList result=new SinglyLinkedList();
        result.head= list.mergeSortedList(list1.head,list2.head);
        result.display();

    }
}
