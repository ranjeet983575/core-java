package DSPractice.sorting;

import java.util.Arrays;

/*
O(N) - best case
O[N2] worst case (N square) and average case
*/
public class BubbleSort {
    public static void main(String[] args) {
        int[] array = {6, 3, 9, 1, -3, 0, 10, 4};
        int[] array1 = sort(array);
        System.out.println(Arrays.toString(array1));
    }

    public static int[] sort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;
    }
}
