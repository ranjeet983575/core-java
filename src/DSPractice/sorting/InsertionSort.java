package DSPractice.sorting;

import java.util.Arrays;

public class InsertionSort {

    public static void main(String[] args) {
        int[] array = {6, 3, 9, 1, -3, 0, 10, 4};
        int[] array1 = sort(array);
        System.out.println(Arrays.toString(array1));
    }

    public static int[] sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int j = i - 1;
            int key = array[i];
            while (j >= 0 && array[j] > key) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = key;
        }
        return array;
    }
}
