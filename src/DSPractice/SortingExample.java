package DSPractice;

import java.util.Arrays;

public class SortingExample {

    public static int[] bubbleSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;
    }

    public static int[] insertionSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int temp = array[i];
            int j = i - 1;
            while (j >= 0 && array[j] > temp) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = temp;
        }
        return array;
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }


    public static int[] mergeTwoSortedArray(int[] a, int[] b, int n, int m) {
        int[] result = new int[n + m];
        int i = 0;
        int j = 0;
        int k = 0;
        while (i < n && j < m) {
            if (a[i] < b[j]) {
                result[k] = a[i];
                i++;
            } else {
                result[k] = b[j];
                j++;
            }
            k++;
        }
        while (i < n) {
            result[k] = a[i];
            i++;
            k++;
        }
        while (j < m) {
            result[k] = b[j];
            j++;
            k++;
        }
        return result;
    }

    public static void mergeSort(int[] arr, int[] temp, int low, int high) {
        if (low < high) {
            int mid = low + (high - low) / 2;
            mergeSort(arr, temp, low, mid);
            mergeSort(arr, temp, mid + 1, high);
            merge(arr, temp, low, mid, high);
        }
    }

    private static void merge(int[] arr, int[] temp, int low, int mid, int high) {
        for (int i = low; i <= high; i++) {
            temp[i] = arr[i];
        }
        int i = low;
        int j = mid + 1;
        int k = low;
        while (i <= mid && j <= high) {
            if (temp[i] <= temp[j]) {
                arr[k] = temp[i];
                i++;
            } else {
                arr[k] = temp[j];
                j++;
            }
            k++;
        }
        while (i <= mid) {
            arr[k] = temp[i];
            k++;
            i++;
        }
    }

    public static int[] sortedSquare(int[] array) {
        int n = array.length;
        int i = 0;
        int j = n - 1;
        int[] result = new int[n];
        for (int k = n - 1; k >= 0; k--) {
            if (Math.abs(array[i]) > Math.abs(array[j])) {
                result[k] = array[i] * array[i];
                i++;
            } else {
                result[k] = array[j] * array[j];
                j--;
            }
        }
        return result;
    }


    public static void main(String[] args) {
        int[] squareArray = {-4, -1, 0, 3, 10};
        // int[] array = {4, 6, 3, 4, 8, 1, 0, 3, 8, 2};
        //int[] ints = bubbleSort(array);
//        int[] ints = insertionSort(array);
        // int[] a = {2, 5, 7};
        //int[] b = {3, 4, 9};
//        printArray(ints);
        //int[] ints1 = mergeTwoSortedArray(a, b, a.length, b.length);
        //printArray(ints1);
        // printArray(array);
        // mergeSort(array, new int[array.length], 0, array.length - 1);
        // printArray(array);
        int[] ints = sortedSquare(squareArray);
        printArray(ints);

    }

}
