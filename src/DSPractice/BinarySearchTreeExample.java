package DSPractice;


class BinarySearchTree {
    private TreeNode root = null;

    static class TreeNode {
        private int data;
        private TreeNode left;
        private TreeNode right;

        public TreeNode(int data) {
            this.data = data;
        }

        public int getData() {
            return data;
        }
    }

    public TreeNode getRoot() {
        return root;
    }


    public TreeNode insertNode(TreeNode root, int data) {
        if (root == null) {
            root = new TreeNode(data);
            return root;
        }
        if (data < root.data) {
            root.left = insertNode(root.left, data);
        } else {
            root.right = insertNode(root.right, data);
        }
        return root;
    }

    public void insert(int value) {
        root = insertNode(root, value);
    }

    public void inOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        inOrder(root.left);
        System.out.print(root.data + " ");
        inOrder(root.right);
    }

    public TreeNode search(TreeNode root, int key) {
        if (root == null || root.data == key)
            return root;
        if (key < root.data) {
            return search(root.left, key);
        } else {
            return search(root.right, key);
        }
    }


}

public class BinarySearchTreeExample {
    public static void main(String[] args) {
        BinarySearchTree bst = new BinarySearchTree();
        bst.insert(5);
        bst.insert(3);
        bst.insert(7);
        bst.insert(1);

        bst.inOrder(bst.getRoot());

        System.out.println("Search Element is : " + bst.search(bst.getRoot(), 3).getData());


    }
}
