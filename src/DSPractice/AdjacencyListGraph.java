package DSPractice;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class AdjacencyListGraph {

    LinkedList<Integer>[] adj;
    private int size;
    private int V;
    private int E;

    AdjacencyListGraph(int nodes) {
        this.adj = new LinkedList[nodes];
        this.V = nodes;
        this.E = 0;
        for (int i = 0; i < nodes; i++) {
            this.adj[i] = new LinkedList<>();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(V + " vertices, " + E + " Edges" + "\n");
        for (int v = 0; v < V; v++) {
            sb.append(v + ":");
            for (int w : adj[v]) {
                sb.append(w + " ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public void addEdge(int u, int v) {
        this.adj[u].add(v);
        this.adj[v].add(u);
        this.E++;
    }

    public void bfs(int s) {
        boolean[] visited = new boolean[V];
        Queue<Integer> q = new LinkedList<>();
        visited[s] = true;
        q.offer(s);
        while (!q.isEmpty()) {
            int u = q.poll();
            System.out.print(u + " ");
            for (int v : adj[u]) {
                if (!visited[v]) {
                    visited[v] = true;
                    q.offer(v);
                }
            }
        }
    }

    public static void main(String[] args) {
        AdjacencyListGraph graph = new AdjacencyListGraph(5);
        System.out.println(graph.toString());
        graph.addEdge(0, 1);
        graph.addEdge(1, 2);
        graph.addEdge(2, 3);
        graph.addEdge(3, 0);
        graph.addEdge(2, 4);

        System.out.println(graph.toString());
        graph.bfs(0);
        graph.bfs(3);

    }

}
