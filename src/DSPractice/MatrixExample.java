package DSPractice;

import java.util.Arrays;

public class MatrixExample {

    public static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }


    public static int[][] addMatrix(int[][] matrix1, int[][] matrix2) {
        int[][] result = new int[3][3];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix2[i].length; j++) {
                result[i][j] = matrix1[i][j] + matrix2[i][j];
            }
            //System.out.println();
        }
        return result;
    }

    public static int[][] subMatrix(int[][] matrix1, int[][] matrix2) {
        int[][] result = new int[3][3];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix2[i].length; j++) {
                result[i][j] = matrix2[i][j] - matrix1[i][j];
            }
            //System.out.println();
        }
        return result;
    }

    public static int[][] multiMatrix(int[][] matrix1, int[][] matrix2) {
        int[][] result = new int[3][3];
        int c[][] = new int[3][3];

        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix2[i].length; j++) {
                c[i][j] = 0;
                for (int k = 0; k < 3; k++) {
                    c[i][j] += matrix1[i][k] * matrix2[k][j];
                }
                result[i][j] = c[i][j];
            }
        }
        return result;
    }

    public static int[][] transposeMatrix(int[][] matrix1) {
        int[][] result = new int[3][3];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1.length; j++) {
                result[j][i] = matrix1[i][j];
            }
        }
        return result;
    }

    public static int[][] diagonalMatrix(int[][] matrix1) {
        int[][] result = new int[3][3];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1.length; j++) {
                if (i == j) {
                    result[j][i] = matrix1[i][j];
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {

        int[][] matrix1 = {{1, 1, 1}, {2, 2, 2}, {3, 3, 3}};
        int[][] matrix2 = {{1, 1, 1}, {2, 2, 2}, {3, 3, 3}};

        printMatrix(matrix1);
//        printMatrix(matrix2);
//        int[][] addition = addMatrix(matrix1, matrix2);
//        int[][] subtraction = subMatrix(matrix1, matrix2);
//        printMatrix(addition);
        //printMatrix(subtraction);

        //print using Array Utility class
        //System.out.println(Arrays.deepToString(subtraction));
//        int[][] multiplication = multiMatrix(matrix1, matrix2);
//        System.out.println(Arrays.deepToString(multiplication));

//        int[][] transpose = transposeMatrix(matrix1);
//        System.out.println(Arrays.deepToString(transpose));

        int[][] diagonal = diagonalMatrix(matrix1);
        printMatrix(diagonal);

    }
}
