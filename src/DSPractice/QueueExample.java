package DSPractice;


import java.util.NoSuchElementException;

class Queue {

    private ListNode front;
    private ListNode rear;
    private int length;

    static class ListNode {
        private int data;
        private ListNode next;

        public ListNode(int data) {
            this.data = data;
            this.next = null;
        }

    }

    public Queue() {
        front = null;
        rear = null;
        length = 0;
    }

    public boolean isEmpty() {
        return length == 0;
    }

    public int length() {
        return length;
    }

    public void enqueue(int data) {
        ListNode newNode = new ListNode(data);
        if (isEmpty()) {
            front = newNode;
        } else {
            rear.next = newNode;
        }
        rear = newNode;
        length++;
    }

    public void display() {
        if (isEmpty()) {
            System.out.print("null");
            return;
        }
        ListNode current = front;
        while (current != null) {
            System.out.print(current.data + " --> ");
            current = current.next;
        }
        System.out.print("null");
        System.out.println();
    }

    public int dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException("Queue is Empty");
        }
        int temp = front.data;
        front = front.next;
        if (front == null) {
            rear = null;
        }
        length--;
        return temp;
    }

    public int first() {
        if (isEmpty()) {
            throw new NoSuchElementException("Queue is Empty");
        }
        return front.data;
    }


    public int last() {
        if (isEmpty()) {
            throw new NoSuchElementException("Queue is Empty");
        }
        return rear.data;
    }

}

public class QueueExample {
    public static void main(String[] args) {

        Queue queue = new Queue();
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(6);
        queue.enqueue(7);
        queue.display();
        System.out.println("Deleted Element is : " + queue.dequeue());
        System.out.println("Deleted Element is : " + queue.dequeue());
        queue.enqueue(8);
        queue.display();
        System.out.println("First Element is : " + queue.first());
        System.out.println("Last Element is : " + queue.last());


    }
}
