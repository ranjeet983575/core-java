package DSPractice.Stack;

public class Stack {

    private Node top;
    private int length;

    Stack() {
        this.top = null;
        this.length = 0;
    }

    public boolean isEmpty() {
        return top == null;
    }

    public void push(int data) {
        Node newNode = new Node(data);
        if (isEmpty()) {
            top = newNode;
            return;
        }
        newNode.next = top;
        top = newNode;
        length++;
    }

    public int pop() {
        if (isEmpty()) {
            System.out.println("Stack is Empty");
            return -1;
        }
        int data = top.data;
        top = top.next;
        length--;
        return data;
    }

    public int peek() {
        if (isEmpty()) {
            System.out.println("Stack is Empty");
            return -1;
        }
        return top.data;
    }

    public void display() {
        if (isEmpty()) {
            System.out.println("Stack is Empty");
            return;
        }
        Node current = top;
        while (current != null) {
            System.out.print(current.data);
            current = current.next;
            System.out.print(" ");
        }
        System.out.println();
    }


}
