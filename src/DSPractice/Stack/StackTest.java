package DSPractice.Stack;

public class StackTest {
    public static void main(String[] args) {

        Stack stack = new Stack();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.display();
        System.out.println(stack.peek());
        System.out.println(stack.pop());
        stack.display();
    }
}
