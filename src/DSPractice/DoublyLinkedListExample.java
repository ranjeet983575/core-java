package DSPractice;

class DoublyLinkedList {

    private ListNode head = null;
    private ListNode tail = null;

    private int length = 0;

    public class ListNode {
        private ListNode previous;
        private ListNode next;

        private int data;

        public ListNode(int data) {
            this.data = data;
        }

        public int getData() {
            return data;
        }
    }

    public DoublyLinkedList() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    public boolean isEmpty() {
        return length == 0;
    }

    public void displayForward() {
        if (head == null) {
            return;
        }
        ListNode temp = head;
        while (temp != null) {
            System.out.print(temp.data + " --> ");
            temp = temp.next;
        }
        System.out.print("null");
        System.out.println("");
    }

    public void displayBackward() {
        if (tail == null) {
            return;
        }
        ListNode temp = tail;
        while (temp != null) {
            System.out.print(temp.data + " --> ");
            temp = temp.previous;
        }
        System.out.print("null");
        System.out.println("");
    }

    public void insertLast(int data) {
        ListNode newNode = new ListNode(data);
        if (isEmpty()) {
            head = newNode;
        } else {
            tail.next = newNode;
        }
        newNode.previous = tail;
        tail = newNode;
        length++;
    }

    public void insertFirst(int data) {
        ListNode newNode = new ListNode(data);
        if (isEmpty()) {
            head = newNode;
        } else {
            head.previous = newNode;
        }
        newNode.next = head;
        head = newNode;
        length++;
    }

    public ListNode deleteFirst() {
        if (isEmpty()) {
            return null;
        }
        ListNode temp = head;
        if (head == tail) {
            tail = null;
        } else {
            head.next.previous = null;
        }
        head = head.next;
        temp.next = null;
        return temp;
    }

    public ListNode deleteLast() {
        if (isEmpty()) {
            return null;
        }
        ListNode temp = tail;
        if (head == tail) {
            head = null;
        } else {
            tail.previous.next = null;
        }
        tail = tail.previous;
        temp.previous = null;
        return temp;
    }

    public void insertAt(int position, int data) {
        if (isEmpty()) {
            System.out.println("Empty List");
            return;
        }
        if (position > length) {
            System.out.println("Invalid Position");
            return;
        }

        ListNode newNode = new ListNode(data);
        ListNode current = head;
        int count = 1;
        while (count < position - 1) {
            current = current.next;
            count++;
        }
        newNode.next = current.next;
        newNode.previous = current;
        current.next = newNode;
    }

    public ListNode deleteAt(int position) {
        if (isEmpty()) {
            System.out.println("Empty List");
            return null;
        }
        if (position > length) {
            System.out.println("Invalid Position");
            return null;
        }
        ListNode current = head;
        int count = 1;
        while (count < position - 1) {
            current = current.next;
            count++;
        }
        ListNode temp = current.next;
        current.next = current.next.next;
        temp.next.previous = temp.previous;
        temp.next = null;
        temp.previous = null;
        return temp;
    }

    public boolean search(int data) {
        if (isEmpty()) {
            System.out.println("List is Empty");
            return false;
        }
        ListNode current = head;
        while (current != null) {
            if (current.data == data) {
                System.out.println("Data Found");
                return true;
            }
            current = current.next;
        }
        return false;
    }

    public ListNode findMiddleNode() {
        if (isEmpty()) {
            return null;
        }
        ListNode slowPtr = head;
        ListNode fastPtr = head;
        while (fastPtr != null && fastPtr.next != null) {
            slowPtr = slowPtr.next;
            fastPtr = fastPtr.next.next;
        }
        return slowPtr;
    }

    public ListNode findNthNode(int position) {
        if (isEmpty()) {
            return null;
        }
        if (position > length) {
            return null;
        }
        ListNode mainPtr = head;
        ListNode refPtr = head;
        int count = 1;
        while (count <= position) {
            refPtr = refPtr.next;
            count++;
        }

        while (refPtr != null) {
            mainPtr = mainPtr.next;
            refPtr = refPtr.next;
        }
        return mainPtr;
    }


}

public class DoublyLinkedListExample {
    public static void main(String[] args) {
        DoublyLinkedList list = new DoublyLinkedList();
        System.out.println(list.isEmpty());

        list.insertLast(1);
        list.insertLast(3);
        list.insertLast(4);
        list.insertLast(6);
        list.displayForward();
        list.displayBackward();
        list.insertLast(7);
        list.insertFirst(0);
        list.displayForward();
        list.displayBackward();
        System.out.println("Deleted First Node is : " + list.deleteFirst().getData());
        list.displayForward();
        System.out.println("Deleted Last Node is : " + list.deleteLast().getData());
        list.displayForward();
        list.insertAt(2, 2);
        list.insertAt(5, 5);
        list.displayForward();
        System.out.println("Deleted Node at position is " + list.deleteAt(3).getData());
        list.displayForward();
        System.out.println(list.search(4));
        System.out.println(list.search(3));
        System.out.println("Middle Node is : " + list.findMiddleNode().getData());
        System.out.println("Nth Node is : " + list.findNthNode(2).getData());


    }
}
