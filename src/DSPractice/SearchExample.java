package DSPractice;

public class SearchExample {

    public static boolean linearSearch(int[] array, int key) {
        for (int x : array) {
            if (x == key)
                return true;
        }
        return false;
    }

    public static int binarySearch(int[] array, int key) {
        int low = 0;
        int high = array.length - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (key == array[mid]) {
                return array[mid];
            }
            if (key < array[mid]) {
                high = mid - 1;
            } else {
                low = low + 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        System.out.println("Linear Search");
        System.out.println("Search Succeed : " + linearSearch(array, 3));
        System.out.println("Search Succeed : " + linearSearch(array, 30));
        System.out.println("---Binary Search");
        System.out.println("Search Succeed : " + binarySearch(array, 3));
        System.out.println("Search Succeed : " + binarySearch(array, 8));
        System.out.println("Search Succeed : " + binarySearch(array, 30));
        System.out.println("Search Succeed : " + binarySearch(array, 0));
    }
}
