package DSPractice;

public class ArrayExample {

    public static void printArray(int[] array) {
        int n = array.length;
        for (int i = 0; i < n; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static int[] removeOdd(int[] array) {
        int n = array.length;
        int evenCount = 0;
        for (int i = 0; i < n; i++) {
            if (array[i] % 2 == 0) {
                evenCount++;
            }
        }

        int[] result = new int[evenCount];
        int index = 0;
        for (int i = 0; i < n; i++) {
            if (array[i] % 2 == 0) {
                result[index] = array[i];
                index++;
            }
        }
        return result;
    }

    public static int[] reverseArray(int[] array) {
        int first = 0;
        int last = array.length - 1;
        //method1
        while (first < last) {
            int temp = array[first];
            array[first] = array[last];
            array[last] = temp;
            first++;
            last--;
        }
        return array;
    }

    public static void findMinimum(int[] array) {
        int min = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }
        System.out.println("MINIMUM IS : " + min);
    }

    public static void findMaximum(int[] array) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException("Invalid Input");
        }

        int max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
        System.out.println("MAXIMUM IS :" + max);
    }

    public static void findSecondMax(int[] array) {
        int max = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                secondMax = max;
                max = array[i];
            } else if (array[i] > secondMax && array[i] != max) {
                secondMax = array[i];
            }
        }
        System.out.println("SECOND MAXIMUM IS :" + secondMax);
    }

    public static void findSecondMin(int[] array) {
        int min = Integer.MAX_VALUE;
        int secondMin = Integer.MAX_VALUE;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                secondMin = min;
                min = array[i];
            } else if (array[i] < secondMin && array[i] != min) {
                secondMin = array[i];
            }
        }
        System.out.println("SECOND MINIMUM IS :" + secondMin);
        //System.out.println("MINIMUM IS :" + min);
    }

    public static int[] shiftZeroEndSameOrder(int[] array) {
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0 && array[j] == 0) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
            if (array[j] != 0) {
                j++;
            }
        }
        return array;
    }

    public static int findMissingNumber(int[] array) {
        int n = array.length + 1;
        int total = n * (n + 1) / 2;
        int sum = 0;
        for (int i = 0; i < n - 1; i++) {
            sum = sum + array[i];
        }
        return total - sum;
    }

    public static boolean isStringPalindrome(String str) {
//        int first = 0;
//        int last = str.length() - 1;
//        while (first < last) {
//            if (str.charAt(first) != str.charAt(last)) {
//                return false;
//            }
//            first++;
//            last--;
//        }
//        return true;

        for (int i = 0, j = str.length() - 1; i <= str.length() - 1 / 2 && j > 0; i++, j--) {
            if (str.charAt(i) != str.charAt(j)) {
                System.out.println(str.charAt(i) + " " + str.charAt(j));
                return false;
            }
        }
        return true;
    }


    public static void main(String[] args) {
        int[] array = {3, 2, 5, 10, 7, 6, 8};
        int[] array1 = {3, 0, 5, 0, 1, 7, 6, 8};
        int[] array2 = {2, 5, 1, 3};

        String name1 = "Ranjeet";
        String str = "madam";

        ArrayExample.printArray(array);
        ArrayExample.printArray(array1);
//        int[] evens = ArrayExample.removeOdd(array);
//        ArrayExample.printArray(evens);

//        int[] reverse = ArrayExample.reverseArray(array1);
//        ArrayExample.printArray(reverse);

//        ArrayExample.findMinimum(array);
//        ArrayExample.findMinimum(array1);

//        ArrayExample.findMaximum(array);
//        ArrayExample.findMaximum(null);

//        ArrayExample.findSecondMax(array);
//        ArrayExample.findSecondMax(array1);

//        ArrayExample.findSecondMin(array);
//        ArrayExample.findSecondMin(array1);

//        int [] zero=ArrayExample.shiftZeroEndSameOrder(array1);
//        ArrayExample.printArray(zero);


//        System.out.println(ArrayExample.findMissingNumber(array2));

        System.out.println(ArrayExample.isStringPalindrome(name1));
        System.out.println(ArrayExample.isStringPalindrome(str));
    }
}
