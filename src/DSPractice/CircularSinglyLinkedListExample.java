package DSPractice;



import java.util.NoSuchElementException;

class CircularSinglyLinkedList {

    private ListNode last;
    private int length;

    static class ListNode {
        private int data;
        private ListNode next;

        public ListNode(int data) {
            this.data = data;
        }

        public int getData() {
            return data;
        }
    }

    public CircularSinglyLinkedList() {
        last = null;
        length = 0;
    }

    public int length() {
        return length;
    }

    public boolean isEmpty() {
        return length == 0;
    }

    public void createList() {
        ListNode first = new ListNode(1);
        ListNode second = new ListNode(2);
        ListNode third = new ListNode(3);

        first.next = second;
        second.next = third;
        third.next = first;
        last = third;
    }

    public void display() {
        if (last == null)
            return;
        ListNode first = last.next;
        while (first != last) {
            System.out.print(first.data + " --> ");
            first = first.next;
        }
        System.out.print(first.data + " ");
        System.out.println();
    }

    public void insertAtFirst(int data) {
        ListNode newNode = new ListNode(data);
        if (last == null) {
            last = newNode;

        } else {
            newNode.next = last.next;
        }
        last.next = newNode;
        length++;
    }

    public void insertAtLast(int data) {
        ListNode newNode = new ListNode(data);
        if (last == null) {
            last = newNode;
            last.next = newNode;
        } else {
            newNode.next = last.next;
            last.next = newNode;
        }
        last = newNode;
        length++;
    }

    public ListNode removeFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        ListNode temp = last.next;
        if (last == last.next) {
            last = null;
            return temp;
        } else {
            last.next = temp.next;
            temp.next = null;
        }
        length--;
        return temp;
    }

    public ListNode removeLast() {
        if (last == null) {
            throw new NoSuchElementException();
        }
        ListNode temp = last;
        ListNode first = last.next;
        while (first.next != last) {
            first = first.next;
        }
        last = first;
        last.next = first.next.next;
        temp.next = null;
        return temp;
    }


}

public class CircularSinglyLinkedListExample {
    public static void main(String[] args) {

        CircularSinglyLinkedList list = new CircularSinglyLinkedList();
        list.createList();
        list.display();
        list.insertAtFirst(0);
        list.display();
        list.insertAtLast(4);
        list.insertAtLast(5);
        list.insertAtLast(6);
        list.insertAtLast(7);
        list.display();
        System.out.println("Deleted Node is : " + list.removeFirst().getData());
        list.display();
        System.out.println("Deleted Node is : " + list.removeFirst().getData());
        list.display();
        System.out.println("Deleted Node is : " + list.removeLast().getData());
        list.display();
        System.out.println("Deleted Node is : " + list.removeLast().getData());
        list.display();

    }

}
