package Pattern;

public class ReversePattern {
	public static void main(String[] args) {

		for (int i = 1; i <= 5; i++) {
			for (int j = 4; j >= i; j--) {
				System.err.print(" ");
			}
			for (int k = 0; k < i; k++) {
				System.err.print("*");
			}
			System.err.println();
		}

		for (int i = 1; i <= 5; i++) {
			for (int j = 0; j < i; j++) {
				System.err.print(" ");
			}
			for (int k = 4; k >= i; k--) {
				System.err.print("*");
			}
			System.err.println();
		}

	}

}
