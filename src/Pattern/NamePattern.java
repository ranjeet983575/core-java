package Pattern;

public class NamePattern {
	public static void main(String[] args) {
		for (int i = 0; i < 5; i++) {

			for (int r = 0; r < 5; r++) {
				if (i == 0 || i == 2 && r != 4 || r == 0 || r == 4 && i != 2)
					System.err.print("*");
				else
					System.err.print(" ");
			}

			for (int s = 0; s < 1; s++) {
				System.err.print(" ");
			}
			for (int a = 0; a < 5; a++) {
				if (i == 0 || i == 2 || a == 0 || a == 4)
					System.err.print("*");
				else
					System.err.print(" ");
			}

			for (int s = 0; s < 1; s++) {
				System.err.print(" ");
			}
			for (int n = 0; n < 5; n++) {
				if (n == 0 || n == 4 || n == i)
					System.err.print("*");
				else
					System.err.print(" ");
			}

			for (int s = 0; s < 1; s++) {
				System.err.print(" ");
			}
			for (int j = 0; j < 5; j++) {
				if (i == 0 || i == 4 || j == 4)
					System.err.print("*");
				else
					System.err.print(" ");
			}

			for (int s = 0; s < 1; s++) {
				System.err.print(" ");
			}
			for (int e = 0; e < 5; e++) {
				if (i == 0 || i == 2 || i == 4 || e == 0)
					System.err.print("*");
				else
					System.err.print(" ");
			}

			for (int s = 0; s < 1; s++) {
				System.err.print(" ");
			}
			for (int e1 = 0; e1 < 5; e1++) {
				if (i == 0 || i == 2 || i == 4 || e1 == 0)
					System.err.print("*");
				else
					System.err.print(" ");
			}
			for (int s = 0; s < 1; s++) {
				System.err.print(" ");
			}
			for (int t = 0; t < 5; t++) {
				if (i == 0 || t == 2)
					System.err.print("*");
				else
					System.err.print(" ");
			}

			for (int s = 0; s < 1; s++) {
				System.err.print(" ");
			}
			for (int p = 0; p < 5; p++) {
				if (i == 2 || p == 2)
					System.err.print("*");
				else
					System.err.print(" ");
			}

			for (int s = 0; s < 1; s++) {
				System.err.print(" ");
			}
			for (int c = 0; c < 5; c++) {
				if (i == 0 || i == 4 || c == 0)
					System.err.print("*");
				else
					System.err.print(" ");
			}

			for (int s = 0; s < 1; s++) {
				System.err.print(" ");
			}
			for (int h = 0; h < 5; h++) {
				if (i == 2 || h == 0 || h == 4)
					System.err.print("*");
				else
					System.err.print(" ");
			}

			for (int s = 0; s < 1; s++) {
				System.err.print(" ");
			}
			for (int h1 = 0; h1 < 5; h1++) {
				if (i == 2 || h1 == 0 || h1 == 4)
					System.err.print("*");
				else
					System.err.print(" ");
			}
			for (int s = 0; s < 1; s++) {
				System.err.print(" ");
			}
			for (int a = 0; a < 5; a++) {
				if (i == 0 || i == 2 || a == 0 || a == 4)
					System.err.print("*");
				else
					System.err.print(" ");
			}
			for (int s = 0; s < 1; s++) {
				System.err.print(" ");
			}
			for (int v = 0; v < 5; v++) {
				if (v==0 && i<=2 || v==4 &&i<=2 ||i==4&&v==2 ||((i==3&&v==1)||(i==3&&v==3))) {
					System.err.print("*");

				} else
					System.err.print(" ");
			}

			for (int s = 0; s < 1; s++) {
				System.err.print(" ");
			}
			for (int t = 0; t < 5; t++) {
				if (i == 0 || t == 2 || i == 4)
					System.err.print("*");
				else
					System.err.print(" ");
			}

			System.err.println();
		}
	}

}
