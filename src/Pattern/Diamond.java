package Pattern;

public class Diamond {
	public static void main(String[] args) {
		for (int i = 0; i < 5; i++) {
			for (int j = 4; j > i; j--) {
				System.err.print(" ");
			}
			for (int j = 1; j <= i + 1; j++) {
				System.err.print("*");
			}
			for (int k = 0; k < i; k++) {
				System.err.print("*");
			}
			System.err.println();
		}

		for (int i = 0; i < 4; i++) {
			for (int j = 1; j <= i + 1; j++) {
				System.err.print(" ");
			}
			for (int j = 3; j > i; j--) {
				System.err.print("*");
			}
			for (int j = 4; j > i; j--) {
				System.err.print("*");
			}
			System.err.println();
		}

	}

}
