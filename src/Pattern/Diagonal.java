package Pattern;

public class Diagonal {
	public static void main(String[] args) {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j <= i; j++) {
				if (i == j) {
					System.err.print("*");
				} else {
					System.err.print(" ");
				}
			}
			for (int k = 5; k > i + 1; k--) {
				System.err.print(" ");
			}
			for (int l = 5; l > i + 1; l--) {
				if (l == i + 2)
					System.err.print("*");
				else
					System.err.print(" ");
			}

			System.err.println();
		}
		for (int i = 0; i < 4; i++) {
			for (int p = 5; p > i + 1; p--) {
				if (p == i + 2)
					System.err.print("*");
				else
					System.err.print(" ");
			}
			for (int q = 0; q <= i; q++) {
				System.err.print(" ");
			}
			for (int r = 0; r <= i; r++) {
				if (r == i) {
					System.err.print("*");
				} else {
					System.err.print(" ");
				}
			}
			System.err.println();
		}

	}

}
