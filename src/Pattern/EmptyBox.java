package Pattern;

public class EmptyBox {
	public static void main(String[] args) {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (i == 0 || j == 0 || i == 4 || j==4) {
					System.err.print(" *");
				} else {
					System.err.print("  ");
				}
			}
			System.err.println();
		}

		System.err.println("filled Box");
		System.err.println();
		
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (i == 0 || j == 0 || i == 4 || j==4) {
					System.err.print(" #");
				} else {
					System.err.print(" $");
				}
			}
			System.err.println();
		}
		
	}

}
