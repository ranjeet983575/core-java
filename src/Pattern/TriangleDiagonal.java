package Pattern;

public class TriangleDiagonal {
	public static void main(String[] args) {

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j <= i; j++) {
				if (i == j)
					System.err.print("*");
				else
					System.err.print(" ");
			}
			System.err.println();
		}
		for (int p = 4; p > 0; p--) {
			for (int q = p; q > 0; q--) {
				if (q==1)
					System.err.print("*");
				else {
					System.err.print(" ");
				}
			}
			System.err.println();
		}

	}

}
