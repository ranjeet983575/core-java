package Pattern;

public class PlusSymbol {
	public static void main(String[] args) {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (i == 2 || j == 2) {
					System.err.print(" *");
				} else {
					System.err.print("  ");
				}
			}
			System.err.println();

		}

	}

}
