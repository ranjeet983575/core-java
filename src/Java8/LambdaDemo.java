package Java8;

/*Interface reference can be used to hold lambda expression
* Using lambda expression we don't need to use any extra calls implementation
* */
public class LambdaDemo {

    public static void main(String[] args) {

        VehicleFI vehicle = () -> {
            return "Ranjeet";
        };

        System.out.println(vehicle.getName());
    }
}
