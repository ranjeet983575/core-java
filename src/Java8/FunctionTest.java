package Java8;

import java.util.function.BiFunction;
import java.util.function.Function;

public class FunctionTest {


    public static void main(String[] args) {

        Function<Integer, Integer> findSquare = (num) -> num * num;
        System.out.println(findSquare.apply(5));

        Function<String, Integer> findLen = (str) -> str.length();
        System.out.println(findLen.apply("Ranjeet Kumar"));

        BiFunction<String, String, Integer> addLen = (str1, str2) -> str1.length() + str2.length();
        System.out.println(addLen.apply("Ranjeet", "Kumar"));


    }


}
