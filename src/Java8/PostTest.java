package Java8;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class PostTest {

    public static void main(String[] args) {

        List<Post> posts = new ArrayList<>();

        Post post1 = new Post(1, "One", "Content of the first post", LocalDateTime.now());
        post1.addComment(new Comment(1, "Alice", "Great post!", LocalDateTime.now()));
        post1.addComment(new Comment(3, "Ranjeet", "Thanks for sharing.", LocalDateTime.now()));
        post1.addComment(new Comment(2, "Bob", "Thanks for sharing.", LocalDateTime.now()));

        Post post2 = new Post(2, "Two", "Content of the second post", LocalDateTime.now());
        post2.addComment(new Comment(3, "Charlie", "Interesting read.", LocalDateTime.now()));
        post2.addComment(new Comment(4, "David", "I learned a lot.", LocalDateTime.now()));

        Post post3 = new Post(3, "One", "Content of the third post", LocalDateTime.now());
        post3.addComment(new Comment(5, "Eve", "Thanks for the insights.", LocalDateTime.now()));
        post3.addComment(new Comment(6, "Frank", "Very informative.", LocalDateTime.now()));

        Post post4 = new Post(4, "Four", "Content of the fourth post", LocalDateTime.now());
        post4.addComment(new Comment(7, "Grace", "Helpful article.", LocalDateTime.now()));
        post4.addComment(new Comment(8, "Heidi", "Good job!", LocalDateTime.now()));

        Post post5 = new Post(1, "Two", "Content of the fifth post", LocalDateTime.now());
        post5.addComment(new Comment(9, "Ivan", "Well explained.", LocalDateTime.now()));
        post5.addComment(new Comment(10, "Judy", "Thank you!", LocalDateTime.now()));

        Post post6 = new Post(6, "Six", "Content of the sixth post", LocalDateTime.now());
        post6.addComment(new Comment(11, "Mallory", "Great tips.", LocalDateTime.now()));
        post6.addComment(new Comment(12, "Niaj", "Very useful.", LocalDateTime.now()));

        posts.add(post4);
        posts.add(post1);
        posts.add(post6);
        posts.add(post3);
        posts.add(post2);
        posts.add(post5);

//        System.out.println(posts);

        Map<String, List<Post>> collect = posts.stream().collect(Collectors.groupingBy(Post::getTitle));
        System.out.println(collect);

    }
}
