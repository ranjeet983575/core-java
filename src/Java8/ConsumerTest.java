package Java8;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ConsumerTest {

    public static void main(String[] args) {

        Consumer<String> name = (str) -> System.out.println(str);
        name.accept("Ranjeet Kumar");

        BiConsumer<String, Integer> nameLen = (nameStr, len) -> System.out.println(nameStr + " : " + len);
        nameLen.accept("Ranjeet",7);
    }
}
