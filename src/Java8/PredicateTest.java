package Java8;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class PredicateTest {

    public static void main(String[] args) {

        Predicate<Integer> canVote = (age) -> age >= 18;

        System.out.println(canVote.test(12));
        System.out.println(canVote.test(18));

        BiPredicate<String, Integer> isValidLen = (str, len) -> str.length() == len;

        System.out.println(isValidLen.test("Hello", 5));
        System.out.println(isValidLen.test("Hello", 9));

    }
}
