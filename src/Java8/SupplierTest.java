package Java8;

import java.util.function.Supplier;

public class SupplierTest {

    public static void main(String[] args) {

        Supplier<String> name = () -> "Ranjeet";

        System.out.println(name.get());
    }
}
