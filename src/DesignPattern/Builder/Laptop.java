package DesignPattern.Builder;

public class Laptop {

    private String ram;
    private String hdd;
    private String cpu;

    @Override
    public String toString() {
        return "Laptop{" +
                "ram='" + ram + '\'' +
                ", hdd='" + hdd + '\'' +
                ", cpu='" + cpu + '\'' +
                ", graphics=" + graphics +
                ", bluetooth=" + bluetooth +
                '}';
    }

    private boolean graphics;
    private boolean bluetooth;

    private Laptop(LaptopBuilder builder) {
        this.ram = builder.ram;
        this.cpu = builder.cpu;
        this.hdd = builder.hdd;

        this.graphics = builder.graphics;
        this.bluetooth = builder.bluetooth;
    }

    public static class LaptopBuilder {
        private String ram;
        private String hdd;
        private String cpu;

        private boolean graphics;
        private boolean bluetooth;

        public LaptopBuilder(String ram, String hdd, String cpu) {
            super();
            this.ram = ram;
            this.cpu = cpu;
            this.hdd = hdd;
        }

        public LaptopBuilder setGraphics(boolean graphics) {
            this.graphics = graphics;
            return this;
        }

        public LaptopBuilder setBluetooth(boolean bluetooth) {
            this.bluetooth = bluetooth;
            return this;
        }

        public Laptop build() {
            return new Laptop(this);
        }
    }

}
