package DesignPattern.Builder;

public class Student {

    private int id;
    private String name;
    private int age;
    private String city;

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                '}';
    }

    public Student(StudentBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.age = builder.age;
        this.city = builder.city;
    }

    static class StudentBuilder {

        private int id;
        private String name;
        private int age;
        private String city;

        public StudentBuilder(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public StudentBuilder setAge(int age) {
            this.age = age;
            return this;
        }

        public StudentBuilder setCity(String city) {
            this.city = city;
            return this;
        }

        public Student build() {
            return new Student(this);
        }

    }


}
