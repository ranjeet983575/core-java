package DesignPattern.Builder;

public class BuilderDemo {
    public static void main(String[] args) {
//        Student s=new StudentBuilder().setId(1001).getStudent();
//        System.out.println(s);

//        Laptop laptopBuilder = new Laptop.LaptopBuilder("8 GB", "500 GB", "i5").build();
//        System.out.println(laptopBuilder);
//
//        Laptop laptopBuilder1 = new Laptop.LaptopBuilder("8 GB", "500 GB", "i5").setGraphics(true).setBluetooth(true).build();
//
//        System.out.println(laptopBuilder1);

        Student s=new Student.StudentBuilder(1,"Ranjeet").build();
        System.out.println(s);

        Student s1=new Student.StudentBuilder(2,"Ankita").setAge(27).setCity("Ranchi").build();
        System.out.println(s1);

    }
}
