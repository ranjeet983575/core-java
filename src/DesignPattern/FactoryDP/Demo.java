package DesignPattern.FactoryDP;


interface Mobile {
    public void details();
}

class Nokia implements Mobile {
    @Override
    public void details() {
        System.out.println("Nokia Mobile");
    }
}

class Iphone implements Mobile {
    @Override
    public void details() {
        System.out.println("Iphone Mobile");
    }
}


class MobileFactory {
    private MobileFactory() {
    }

    public static Mobile getMobile(String type) {
        if (type == "cheap") {
            return new Nokia();
        } else {
            return new Iphone();
        }
    }

}

public class Demo {
    public static void main(String[] args) {

        Mobile mobile = MobileFactory.getMobile("cheap");
        mobile.details();


    }
}
