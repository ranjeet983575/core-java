package DesignPattern.FactoryDP;

public class FactoryDemo {
    public static void main(String[] args) {

        OSFactory of=new OSFactory();
        OS open = of.getInstance("close");
        open.spec();
    }
}
