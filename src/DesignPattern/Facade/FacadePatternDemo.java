package DesignPattern.Facade;

public class FacadePatternDemo {
    public static void main(String[] args) {

        DVDPlayer dvdPlayer = new DVDPlayer();
        SoundSystem soundSystem = new SoundSystem();
        Projector projector = new Projector();

        HomeTheaterFacade homeTheater = new HomeTheaterFacade(dvdPlayer, soundSystem, projector);

        homeTheater.startMovie("Inception");
        homeTheater.endMovie();
    }
}
