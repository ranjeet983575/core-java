package DesignPattern.AdapterDP;

public interface Pen {

    public void write(String str);
}
