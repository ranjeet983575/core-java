package DesignPattern.AdapterDP;

public class Asignment {

    Pen p;

    public Pen getP() {
        return p;
    }

    public void setP(Pen p) {
        this.p = p;
    }

    public void writeAsignment(String str)
    {
        p.write(str);
    }
}
