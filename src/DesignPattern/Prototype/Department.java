package DesignPattern.Prototype;

import java.util.ArrayList;
import java.util.List;

public class Department implements Cloneable {

    private int deptId;
    private List<Employee> employees;

    public Department(int deptId) {
        this.deptId = deptId;
        this.employees = getEmployeeFromDb(deptId);
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> getEmployeeFromDb(int deptId) {
        List<Employee> employees = new ArrayList<>();

        Employee e1 = new Employee(101, "A");
        Employee e2 = new Employee(102, "B");
        employees.add(e1);
        employees.add(e2);
        return employees;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "Department{" +
                "deptId=" + deptId +
                ", employees=" + employees +
                '}';
    }
}
