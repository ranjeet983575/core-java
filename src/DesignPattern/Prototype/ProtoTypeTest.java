package DesignPattern.Prototype;

public class ProtoTypeTest {
    public static void main(String[] args) throws CloneNotSupportedException {

        Department dept1 = new Department(1);
        Department dept2 = new Department(2);

        Department dept3 = (Department) dept1.clone();
        dept3.setDeptId(3);

        System.out.println(dept1);
        System.out.println(dept2);
        System.out.println(dept3);


    }
}
