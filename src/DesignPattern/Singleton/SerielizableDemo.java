package DesignPattern.Singleton;

import java.io.*;

public class SerielizableDemo {

    public static void main(String[] args) throws IOException {

        ObjectOutput objectOutput = null;
        SingletonSerielizable singleton1 = SingletonSerielizable.getInstance();
        SingletonSerielizable singleton2 = null;
        try {
            objectOutput = new ObjectOutputStream(new FileOutputStream("singleton.ser"));
            objectOutput.writeObject(singleton1);

            objectOutput.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(objectOutput != null)
                objectOutput.close();
        }

        ObjectInput objectInput = null;

        try {
            objectInput = new ObjectInputStream(new FileInputStream("singleton.ser"));

            Object readObject = objectInput.readObject();

            singleton2 = (SingletonSerielizable)readObject;

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(objectInput != null)
                objectInput.close();
        }

        /**
         * hascode of original singleton
         * object(singleton1) and serialized
         * object(singleton2) are same
         * so singleton has not broken.
         */
        System.out.println(singleton1.hashCode());
        System.out.println(singleton2.hashCode());
    }
}
