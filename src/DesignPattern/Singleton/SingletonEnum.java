package DesignPattern.Singleton;

/**
 * Singleton Class using Enum
 * @author Ranjeet Kumar
 */
public enum SingletonEnum {

    /**
     * This Singleton can be access globally
     */
    GETINSTANCE;

    public String welcome() {
        return "Singleton!!";
    }
}