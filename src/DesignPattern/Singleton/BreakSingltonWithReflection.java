package DesignPattern.Singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class BreakSingltonWithReflection {


        public static void main(String[] args) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InvocationTargetException {

            SingletonLazy singleton = SingletonLazy.getInstance();
            SingletonLazy singleton1 = SingletonLazy.getInstance();
            SingletonLazy singleton2 = null;

            Constructor<?>[] constructors = SingletonLazy.class.getDeclaredConstructors();
            for (Constructor<?> constructor : constructors) {
                constructor.setAccessible(true);

                Object object = constructor.newInstance();
                singleton2 = (SingletonLazy)object;

                break;
            }

            System.out.println(singleton1.hashCode());
            System.out.println(singleton.hashCode());
            System.out.println(singleton2.hashCode());
        }

    }

