package DesignPattern.Singleton;


import java.rmi.server.ExportException;

class SingleTone implements Cloneable {

    private static SingleTone INSTANCE = null;

    private SingleTone() {
        System.out.println("SingleTo Constructor");
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return INSTANCE;
    }

    public static synchronized SingleTone getInstance() {
        if (INSTANCE == null) {
            synchronized (SingleTone.class) {
                try {
                    INSTANCE = new SingleTone();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return INSTANCE;
    }

}


public class Demo {
    public static void main(String[] args) throws CloneNotSupportedException {

        SingleTone instance = SingleTone.getInstance();
        SingleTone instance1 = SingleTone.getInstance();
        SingleTone instance2 = (SingleTone) instance1.clone();

        System.out.println(instance.hashCode() + " " + instance1.hashCode() + " " + instance2.hashCode());

    }

}
