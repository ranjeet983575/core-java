package DesignPattern.Singleton;

public class SingletonBillPugh {

    private SingletonBillPugh() {
        if (SingletonHolder.INSTANCE != null) {
            throw new RuntimeException("Instance already existing !");
        }
    }

    private static class SingletonHolder {
        private static final SingletonBillPugh INSTANCE = new SingletonBillPugh();
    }

    public static SingletonBillPugh getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("Can not clone");
    }

    
}

