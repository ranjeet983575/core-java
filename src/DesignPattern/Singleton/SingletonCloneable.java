package DesignPattern.Singleton;

public class SingletonCloneable implements Cloneable {

    private static SingletonCloneable INSTANCE = null;

    private SingletonCloneable() {

    }
    public static SingletonCloneable getInstance() {
        //If Instance is null then only instantiate
        if (INSTANCE == null) {
            INSTANCE = new SingletonCloneable();
        }
        return INSTANCE;

    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        throw  new CloneNotSupportedException("You can not clone Singleton class");
    }
}
