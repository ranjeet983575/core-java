package DesignPattern.Singleton;

public class SingletonLazy {
    /**
     * Singleton Class using Lazy initialization
     *
     * @author Ranjeet Kumar
     */

    private static SingletonLazy INSTANCE = null;

    /*
     * private constructor so that
     * preventing instance creation from other class
     */
    private SingletonLazy() {

        //Make reflection proof
        if (INSTANCE != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    /**
     * This method has global access to return
     * Single instance within JVM
     *
     * @return Singleton Instance
     */
    public static SingletonLazy getInstance() {
        //If Instance is null then only instantiate
        if (INSTANCE == null) {
            INSTANCE = new SingletonLazy();
        }
        return INSTANCE;

    }
}
