package DesignPattern.Singleton;

import java.io.Serializable;

/**
 * Singleton Class implements Serializable
 *
 * @author Ranjeet Kumar
 */
public class SingletonSerielizable implements Serializable {

    private static final long serialVersionUID = 1808144385520819134L;
    private static SingletonSerielizable INSTANCE = null;

    /*
     * private constructor so that
     * preventing instance creation from other class
     */
    private SingletonSerielizable() {
        //Make reflection proof
        if (INSTANCE != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    /**
     * This method has global access to return
     * Single instance within JVM
     *
     * @return Singleton Instance
     */
    public static SingletonSerielizable getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SingletonSerielizable();
        }
        return INSTANCE;
    }

    /**
     * This method protecting to break singleton
     * by using Serialization process
     *
     * @return return singleton instance
     */
    public Object readResolve() {
        return getInstance();
    }
}