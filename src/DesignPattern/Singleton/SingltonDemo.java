package DesignPattern.Singleton;

public class SingltonDemo {
    public static void main(String[] args) throws CloneNotSupportedException {

        /**
         * Calling getInstance() method many times
         * returns same hashcode that proves that
         * Singleton class creates only one instance
         */
        SingletonEager singleton1 = SingletonEager.getInstance();
        SingletonEager singleton2 = SingletonEager.getInstance();

        SingletonLazy singleton3 = SingletonLazy.getInstance();
        SingletonLazy singleton4 = SingletonLazy.getInstance();

        SingletonThreadSafe singleton5 = SingletonThreadSafe.getInstance();
        SingletonThreadSafe singleton6 = SingletonThreadSafe.getInstance();

        SingletonBillPugh singleton7 = SingletonBillPugh.getInstance();
        SingletonBillPugh singleton8 = SingletonBillPugh.getInstance();

        SingletonEnum singleton9 = SingletonEnum.GETINSTANCE;
        SingletonEnum singleton10 = SingletonEnum.GETINSTANCE;

        System.out.println(singleton1.hashCode());
        System.out.println(singleton2.hashCode());

        System.out.println(singleton3.hashCode());
        System.out.println(singleton4.hashCode());

        System.out.println(singleton5.hashCode());
        System.out.println(singleton6.hashCode());

        System.out.println(singleton7.hashCode());
        System.out.println(singleton8.hashCode());

        System.out.println(singleton9.hashCode());
        System.out.println(singleton10.hashCode());

        SingletonCloneable singleton11 = SingletonCloneable.getInstance();
        SingletonCloneable singleton12 = (SingletonCloneable) singleton11.clone();

        System.out.println(singleton11.hashCode());
        System.out.println(singleton12.hashCode());



    }

}
