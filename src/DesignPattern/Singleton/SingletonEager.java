package DesignPattern.Singleton;

/**
 * Singleton Class using Eager initialization
 * @author Ranjeet Kumar
 */
public class SingletonEager {

    //Eager initialization
    private static final SingletonEager INSTANCE = new SingletonEager();

    /*
     * private constructor so that
     * preventing instance creation from other class
     */
    private SingletonEager() {

    }

    /**
     * This method has global access to return
     * Single instance within JVM
     * @return Singleton Instance
     */
    public static SingletonEager getInstance() {
        return INSTANCE;
    }
}
