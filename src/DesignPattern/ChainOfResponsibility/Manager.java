package DesignPattern.ChainOfResponsibility;

public abstract class Manager {

    protected Manager manager;
    int approvalLimit;
    String managerName;

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public void approveSalary(int employeeSalary) {
        if (this.approvalLimit > employeeSalary) {
            processSalary(employeeSalary);
        } else if (manager != null) {
            manager.approveSalary(employeeSalary);
        } else {
            System.out.println("Salary Can Not be approved");
        }
    }

    public abstract void processSalary(int employeeSalary);
}
