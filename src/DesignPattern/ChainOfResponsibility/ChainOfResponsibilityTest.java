package DesignPattern.ChainOfResponsibility;

public class ChainOfResponsibilityTest {

    public static void main(String[] args) {


        ChainCreator chainCreator = new ChainCreator();
        Manager manager = chainCreator.createChain();

        manager.approveSalary(5000);
        manager.approveSalary(15000);
        manager.approveSalary(30000);
        manager.approveSalary(5000000);

    }
}
