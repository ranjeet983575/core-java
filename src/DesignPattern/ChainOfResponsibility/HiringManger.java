package DesignPattern.ChainOfResponsibility;

public class HiringManger extends Manager {

    public HiringManger(String mangerName, int approvalLimit) {
        this.managerName = mangerName;
        this.approvalLimit = approvalLimit;
    }

    @Override
    public void processSalary(int employeeSalary) {
        System.out.println(this.managerName + " has Approved Salary on Level 1 for Amount " + employeeSalary);
    }
}