package DesignPattern.ChainOfResponsibility;

public class ChainCreator {

    public Manager createChain() {

        HiringManger hiringManger = new HiringManger("Hiring", 10000);
        SeniorManger seniorManger = new SeniorManger("Senior", 20000);
        Director director = new Director("Director", 40000);

        hiringManger.setManager(seniorManger);
        seniorManger.setManager(director);
        return hiringManger;
    }
}
