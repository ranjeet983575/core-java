package DesignPattern.ChainOfResponsibility;

public class SeniorManger extends Manager {

    public SeniorManger(String mangerName, int approvalLimit) {
        this.managerName = mangerName;
        this.approvalLimit = approvalLimit;
    }

    @Override
    public void processSalary(int employeeSalary) {
        System.out.println(this.managerName + " has Approved Salary on Level 1 for Amount " + employeeSalary);
    }
}