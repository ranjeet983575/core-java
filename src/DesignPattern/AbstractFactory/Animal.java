package DesignPattern.AbstractFactory;

public interface Animal {
    public void speak();
}
