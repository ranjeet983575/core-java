package DesignPattern.AbstractFactory;

public class LandAnimalFactory extends AnimalFactory {
    @Override
    public Animal getAnimal(String animalType) {
        if ("dog".equals(animalType)) {
            return new Dog();
        }
        if ("cat".equals(animalType)) {
            return new Cat();
        }
        return null;
    }
}
