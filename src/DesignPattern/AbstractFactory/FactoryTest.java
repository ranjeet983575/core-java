package DesignPattern.AbstractFactory;

public class FactoryTest {
    public static void main(String[] args) {

        AnimalFactory factory = AnimalFactory.getAnimalFactory("sea");
        Animal shark = factory.getAnimal("shark");
        shark.speak();

        AnimalFactory factory1 = AnimalFactory.getAnimalFactory("land");
        Animal dog = factory1.getAnimal("dog");
        dog.speak();


    }
}
