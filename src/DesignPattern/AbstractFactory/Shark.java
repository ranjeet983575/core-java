package DesignPattern.AbstractFactory;

public class Shark implements Animal {
    @Override
    public void speak() {
        System.out.println("Can Not Speak");
    }
}
