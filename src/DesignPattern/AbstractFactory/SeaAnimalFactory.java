package DesignPattern.AbstractFactory;

public class SeaAnimalFactory extends AnimalFactory {
    @Override
    public Animal getAnimal(String animalType) {
        if ("shark".equals(animalType)) {
            return new Shark();
        }
        return null;
    }
}
