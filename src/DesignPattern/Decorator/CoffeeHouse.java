package DesignPattern.Decorator;

public class CoffeeHouse {

    public static void main(String[] args) {

        Beverage beverage = new Espresso();
        System.out.println(beverage.getBeverageName() + " : " + beverage.getBeveragePrice());

        beverage = new Milk(beverage);
        System.out.println(beverage.getBeverageName() + " : " + beverage.getBeveragePrice());

        beverage=new Caramel(beverage);
        System.out.println(beverage.getBeverageName() + " : " + beverage.getBeveragePrice());


    }
}
