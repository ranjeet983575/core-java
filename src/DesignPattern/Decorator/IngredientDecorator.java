package DesignPattern.Decorator;

public abstract class IngredientDecorator extends Beverage {
    abstract String getBeverageName();
}
