package Tricky;

class Cat {
	public static void meow() {
		System.err.println("meow");
	}
}

class Cat2 extends Cat {
	public static void meow() {
		System.err.println("yeow");
	}

}

public class StaticMethodOverrid {
	public static void main(String[] args) {
		Cat c1 = new Cat();
		Cat c2 = new Cat2();

		c1.meow();
		c2.meow();

	}

}
