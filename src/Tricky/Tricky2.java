package Tricky;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Tricky2 {
		
	public static void main(String[] args) {
		SimpleDateFormat dateInput=new SimpleDateFormat("dd/MM/yyyy G");
		SimpleDateFormat dateOutput=new SimpleDateFormat("yyyy/MM/dd G");
		
		try {
			Date date=dateInput.parse("31/1/2016 AD"); 
			//Date date=dateInput.parse("34/1/2016 AD");
			System.err.println(date);
			
			String output=dateOutput.format(date);
			System.out.println(output);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}
}
