package Tricky;

public class IntegerCache {
	public static void main(String[] args) {
		/* -128 to 172 integer maintain the cache*/
		
		Integer a=127;
		Integer b=127;
		
		System.err.println(a==b); //true
		
		Integer c=128;
		Integer d=128;
		
		System.err.println(c==d); //false
		System.err.println(c.equals(d));
		System.err.println(c.compareTo(d));
		
		
	}

}
