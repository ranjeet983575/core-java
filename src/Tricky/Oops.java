package Tricky;

class ParentOops {
    public void parentMsg() {
        System.out.println("Hello Parent");
    }
}

class ChildOops extends ParentOops {
    public void childMsg() {
        System.out.println("Hello Child");
    }

    public void parentMsg() {
        System.out.println("Hello Parent from Child");
    }
}

public class Oops {

    public static void main(String[] args) {
        ChildOops c1=new ChildOops();
        ParentOops c2=new ChildOops();

        c1.childMsg();
        c2.parentMsg();
    }

}
