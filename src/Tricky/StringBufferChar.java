package Tricky;

import java.util.Random;

public class StringBufferChar {
	private static Random rnd = new Random();

	public static void main(String[] args) {
		StringBuffer word = null;
		switch (rnd.nextInt(5)) {
		case 1:
			word = new StringBuffer('P');
		case 2:
			word = new StringBuffer('G');
		case 3:
		default:
			word = new StringBuffer('M');
			break;
		}
		/*
		 * that switch case will fail constructor will create Integer buffer because
		 * there is no constructor with character
		 */

		word.append("a");
		word.append("i");
		word.append("n");
		System.err.println(word);

	}

}
