package Tricky;

public class FinallyPower {
	public static void main(String[] args) {
		System.err.println(test());
	}

	public static int test() {
		try {
			return 5;
		} finally {
			return 0;
		}
	}
}
