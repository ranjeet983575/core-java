package Tricky;

import java.math.BigDecimal;

public class DoubleAndBigDecimal {
	public static void main(String[] args) {
		double d = 1.10 - 1.00;
		System.err.println("double d=" + d);
		if (d == 0.10) {
			System.err.println("Hello");
		} else {
			System.err.println("Hi");
		}

		BigDecimal d1 = new BigDecimal("1.10").subtract(new BigDecimal("1.00"));
		System.err.println("Big Decimal d=" + d1);
		if (d1.doubleValue() == 0.10) {
			System.err.println("Hello");
		} else {
			System.err.println("Hi");
		}

	}

}
