package Tricky;

class A {
	public static void staticMethod() {
		System.err.println("static method called");
	}
}

public class NullWithStatic {
	public static void main(String[] args) {
		A a = null;
		a.staticMethod();
		/*We will not get null pointer exception because method is satic so calling will be converted by the
		 * JVM like A.staticMethod()*/
	}
}
