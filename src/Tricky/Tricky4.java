package Tricky;

//run time exception because after expresion of condition the null get chance

public class Tricky4 {
	static int foo(int i) {
		return (i < 10 ? null : i);
	}

	public static void main(String[] args) {
		System.err.println(foo(1));
	}

}
