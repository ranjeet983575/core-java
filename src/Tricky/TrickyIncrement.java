package Tricky;

class A1 {
	static int i = 100;

	static {
		i = i-- - --i; // 100-98=2
		System.err.println("A1 static =" + i);
	}
	{
		i = i++ + ++i; // 0+2=2
		System.err.println("A1 " + i);
	}
}

class B1 extends A1 {
	static {
		i = --i - i--; // 1-1=0
		System.err.println("B1 Static =" + i);
	}

	{
		i = ++i + i++;// 3+3
		System.err.println("B1 =" + i);
	}
}

public class TrickyIncrement {
	public static void main(String[] args) {
		B1 b=new B1();
	}
}
