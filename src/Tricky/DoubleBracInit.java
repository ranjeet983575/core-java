package Tricky;

import java.util.HashSet;
import java.util.Set;

public class DoubleBracInit {
	public static void main(String[] args) {
		Set<String> valid = new HashSet<>();
		valid.add("A");
		valid.add("B");

		Set<String> valid1 = new HashSet<String>() {
			{
				add("A");
				add("B");
			}
		};
		System.err.println(valid);
		System.err.println(valid1);
	}
}
