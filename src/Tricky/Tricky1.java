package Tricky;

public class Tricky1 {
	public static void main(String[] args) {

		System.err.println("Tricky1 x= " + Test.x);
	}

}

class Test {
	public static final int x = 100;
	static {
		System.out.println("static of Test");
	}
}
/*
 * idealy the staic block then the main method should executed but in this code
 * x is static variable hence while initializing by the jvm it will replace with the actual value
 * so no need of executing Test class hence no static block will executed*/
