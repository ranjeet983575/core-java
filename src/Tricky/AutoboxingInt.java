package Tricky;

public class AutoboxingInt {

	public static void main(String[] args) {
		Long l=new Long(10);
		System.err.println(l.equals(10)); // passing long to int after autoboxing become Integer
	}
}
