package Tricky;

public class ConstructorOverloading {

	public ConstructorOverloading(Object o) {
		System.err.println("Object");
	}

	public ConstructorOverloading(double[] doubleArray) {
		System.err.println("Double");
	}

//	public ConstructorOverloading(int[] intrray) {
//		System.err.println("Double");
//	}

	/* Same case For Methods*/ 
	public static void main(String[] args) {
		ConstructorOverloading c = new ConstructorOverloading(null);
	}
}
