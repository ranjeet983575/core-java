package String;

import java.util.LinkedHashMap;
import java.util.Map;

public class SecondNonRepeating {

    public static void main(String[] args) {
        String str = "swiss";
        System.out.println(findSecondNonRepeating(str));
    }

    public static Character findSecondNonRepeating(String str) {
        Map<Character, Integer> temp = new LinkedHashMap<>();
        for (int i = 0; i < str.length(); i++) {
            temp.put(str.charAt(i), temp.getOrDefault(str.charAt(i), 0) + 1);
        }
        int count = 0;
        for (Map.Entry<Character, Integer> next : temp.entrySet()) {
            if (next.getValue() == 1) {
                count = count + 1;
                if (count == 2) {
                    return next.getKey();
                }
            }
        }
        return null;
    }
}
