package String;

public class StringReverse {

    public static void main(String[] args) {
        System.out.println(reverse("Ranjeet"));
        String reverse = reverseStr("Ranjeet", 6);
        System.out.println(reverse);

    }

    public static String reverse(String str) {
        char[] strArray = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int i = str.length() - 1; i >= 0; i--) {
            sb.append(strArray[i]);
        }
        return sb.toString();
    }

    public static String reverseStr(String str, int len) {
        if (len < 0) {
            return "";
        } else {
            return str.charAt(len) + reverseStr(str, len - 1);
        }
    }


}