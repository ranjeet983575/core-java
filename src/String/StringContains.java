package String;

public class StringContains {

	public static void main(String[] args) {
		
		String str1="Hello World";
		String str2="Hello";
		String str3="ello";
		String str4="o";
		
		Boolean r1=str1.contains(str2);
		Boolean r2=str1.contains(str3);
		Boolean r3=str1.contains(str4);
		
		System.out.println(r1);
		System.out.println(r2);
		System.out.println(r3);

	}

}
