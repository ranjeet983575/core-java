package HackerRank;

class Parent {
    private void display() {
        System.out.println("In Parent");
    }

    public void display1() {
        System.out.println("In Parent 1");
    }
}

class Child extends Parent {
    public void display() {
        System.out.println("In Child");
    }

    public void display2() {
        System.out.println("In Child2");
    }

}

public class OopsProblem {
    public static void main(String[] args) {
        Parent p = new Child();


    }
}


