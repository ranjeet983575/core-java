package HackerRank;

import java.util.Set;
import java.util.TreeSet;

class Person1  {
    String name;

    public Person1(String name) {
        this.name = name;
    }

//    public int compareTo(Object o) {
//        return 0;
//    }
}

public class SetProblem {
    public static void main(String[] args) {
        Person1 p1=new Person1("ABC");
        Person1 p2=new Person1("BCD");
        Person1 p3=new Person1("DEF");
        Person1 p4=new Person1("ABC");

        Set<Person1> t=new TreeSet<Person1>();
        t.add(p1);
        t.add(p2);
        t.add(p3);
        t.add(p4);
        System.out.println(t.size());
    }

}
