package Multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestFixedThreadPool {

    public static void main(String[] args) {

        Runnable task1 = new Runnable() {
            @Override
            public void run() {
                System.out.println("Task1 Started");
                System.out.println(Thread.currentThread().getName());
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Task1 Ends");
            }
        };

        Runnable task2 = () -> {
            System.out.println("Task2 Started");
            System.out.println(Thread.currentThread().getName());
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Task2 Ends");
        };

        Runnable task3 = () -> {
            System.out.println("Task3 Started");
            System.out.println(Thread.currentThread().getName());
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Task3 Ends");
        };


        ExecutorService executorService = Executors.newFixedThreadPool(3);
        executorService.submit(task1);
        executorService.submit(task2);
        executorService.submit(task3);
        executorService.shutdown();

    }


}
