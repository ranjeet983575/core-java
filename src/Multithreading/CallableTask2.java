package Multithreading;

import java.util.concurrent.Callable;

public class CallableTask2 implements Callable<Integer> {

    int input;

    public CallableTask2(int input) {
        this.input = input;
    }

    @Override
    public Integer call() throws Exception {
        int sum = 0;
        for (int i = 1; i <= input; i++) {
            sum += i;
        }
        return sum;
    }
}
