package Multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestExecutorService {


    public static void main(String[] args) {

        Runnable task1 = new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
                System.out.println("Task 1");
            }
        };

        Runnable task2 = () -> {
            System.out.println(Thread.currentThread().getName());
            System.out.println("Task 1");
        };


        ExecutorService executorService = Executors.newSingleThreadExecutor();

        executorService.submit(task1);
        executorService.submit(task2);
        executorService.shutdown();

    }
}
