package Multithreading;

import java.util.concurrent.*;

public class TestCallable {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        Callable task1 = () -> "First Task 1 for callable";

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future submit = executorService.submit(task1);
        System.out.println(submit.get());

        Future<Integer> submit1 = executorService.submit(new CallableTask2(10));
        System.out.println(submit1.get());

        executorService.shutdown();


    }
}
