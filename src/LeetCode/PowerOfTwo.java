package LeetCode;

public class PowerOfTwo {
    public static void main(String[] args) {

        System.out.println("is Power of 2 : " + isPowerOf2(16));
        System.out.println("is Power of 2 : " + isPowerOf2(18));
        System.out.println("is Power of 2 : " + isPowerOf2(32));
        System.out.println("is Power of 2 : " + isPowerOf2(4));


    }

    public static boolean isPowerOf2(int num) {
        int i = 1;
        while (i < num) {
            i = i * 2;
        }
        return num == i;
    }
}
