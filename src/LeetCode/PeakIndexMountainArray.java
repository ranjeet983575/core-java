package LeetCode;

public class PeakIndexMountainArray {

    public static void main(String[] args) {

        // Example Test Case
        int[] arr = {0, 2, 3, 4, 5, 3, 1};
        System.out.println("Peak Index: " + peakIndexInMountainArray(arr)); // Output: 4

        // Additional Test Cases
        int[] arr2 = {0, 1, 0};
        System.out.println("Peak Index: " + peakIndexInMountainArray(arr2)); // Output: 1

        int[] arr3 = {0, 10, 5, 2};
        System.out.println("Peak Index: " + peakIndexInMountainArray(arr3)); // Output: 1

        int[] arr4 = {3, 4, 5, 1};
        System.out.println("Peak Index: " + peakIndexInMountainArray(arr4)); // Output: 2
    }

    public static int peakIndexInMountainArray(int[] arr) {
        int left = 0;
        int right = arr.length - 1;
        while (left < right) {
            int mid = (right+left) / 2;
            if (arr[mid] < arr[mid + 1]) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return left;
    }

}












