package LeetCode;

public class ExcelSheetColumn {

    public static void main(String[] args) {

        System.out.println("Column Num is : " + titleToNumber("AA"));
        System.out.println("Column Num is : " + titleToNumber("AB"));
        System.out.println("Column Num is : " + titleToNumber("BB"));
        System.out.println("Column Num is : " + titleToNumber("ZA"));

    }

    public static int titleToNumber(String column) {
        char[] charArray = column.toCharArray();
        int result = 0;

        for (int i = 0; i < charArray.length; i++) {
            int num = charArray[i] - 65 + 1;
            result = result * 26 + num;
        }

        return result;
    }
}
