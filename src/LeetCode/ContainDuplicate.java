package LeetCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ContainDuplicate {

    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 1};
        int[] nums1 = {1, 2, 3};
        int[] nums2 = {1, 2, 3, 3};

//        System.out.println("Is contain duplicates : " + findWithBrutForce(nums));
//        System.out.println("Is contain duplicates : " + findWithBrutForce(nums1));
//        System.out.println("Is contain duplicates : " + findWithBrutForce(nums2));

        System.out.println("Is contain duplicates : " + findWithJava8(nums));
        System.out.println("Is contain duplicates : " + findWithJava8(nums1));
        System.out.println("Is contain duplicates : " + findWithJava8(nums2));

    }

    public static boolean findWithBrutForce(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] == nums[j]) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean findWithJava8(int[] nums) {
        Integer[] integerArray = Arrays.stream(nums).boxed().toArray(Integer[]::new);
        List<Integer> list = Arrays.asList(integerArray);
        List<Integer> collect = list.stream().distinct().collect(Collectors.toList());
        return collect.size() != nums.length;
    }

}
