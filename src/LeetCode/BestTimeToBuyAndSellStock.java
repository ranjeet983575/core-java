package LeetCode;

public class BestTimeToBuyAndSellStock {
    public static void main(String[] args) {

        int[] prices = {7, 1, 5, 3, 6, 4};
        int[] prices1 = {7, 6, 4, 3, 1};
        int[] prices2 = {1, 2, 3, 4, 5};
        int[] prices3 = {3, 3, 5, 0, 0, 3, 1, 4};
        int[] prices4 = {2, 4, 1};

        System.out.println(findMaxProfit(prices));
        System.out.println(findMaxProfit(prices1));
        System.out.println(findMaxProfit(prices2));
        System.out.println(findMaxProfit(prices3));
        System.out.println(findMaxProfit(prices4));

    }

    private static int findMaxProfit(int[] prices) {
        int minPrice = Integer.MAX_VALUE;
        int maxProfit = 0;

        for (int price : prices) {
            if (price < minPrice) {
                minPrice = price;
            } else if (price - minPrice > maxProfit) {
                maxProfit = price - minPrice;
            }
        }
        return maxProfit;
    }
}
