package LeetCode;

public class FindMinimumInRotatedSortedArray {

    public static void main(String[] args) {

        int[] nums1 = {3, 4, 5, 1, 2};
        System.out.println("Test Case 1 Output: " + findMin(nums1));

        // Test Case 2
        int[] nums2 = {4, 5, 6, 7, 0, 1, 2};
        System.out.println("Test Case 2 Output: " + findMin(nums2));

        // Test Case 3
        int[] nums3 = {11, 13, 15, 17};
        System.out.println("Test Case 3 Output: " + findMin(nums3));

        int[] nums4 = {2, 3, 4, 5, 6, 7, 0, 1};
        System.out.println("Test Case 4 Output: " + findMin(nums4));  // Output: 0

        // Test Case 5
        int[] nums5 = {1};
        System.out.println("Test Case 5 Output: " + findMin(nums5));


    }

//    private static int findMin(int[] nums3) {
//        int minValue = Integer.MAX_VALUE;
//        for (int i = 0; i < nums3.length; i++) {
//            if (minValue > nums3[i]) {
//                minValue = nums3[i];
//            }
//        }
//        return minValue;
//    }

    private static int findMin(int[] nums) {
        int low = 0;
        int high = nums.length - 1;
        while (low < high) {
            int mid = (low + high) / 2;
            if (nums[mid] > nums[high]) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }
        return nums[low];
    }
}
