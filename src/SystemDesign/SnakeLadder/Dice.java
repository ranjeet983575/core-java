package SystemDesign.SnakeLadder;

import java.util.Objects;

public class Dice {

    private int numberOfDice;

    public Dice(int numberOfDice) {
        this.numberOfDice = numberOfDice;
    }

    public int rollDice() {
        return ((int) (Math.random() * (6 * numberOfDice - 1 * numberOfDice))) + 1;
    }

    public int getNumberOfDice() {
        return numberOfDice;
    }

    public void setNumberOfDice(int numberOfDice) {
        this.numberOfDice = numberOfDice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dice dice = (Dice) o;
        return numberOfDice == dice.numberOfDice;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(numberOfDice);
    }
}
