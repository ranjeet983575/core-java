package SystemDesign.SnakeLadder;

import java.util.*;

/**
 * 1. Multiplayer game
 * 2. Customizable board in items of snake and ladder placement and board size
 * 3. Customizable in the term of number of dice used
 * 4. When Game board loads it should:-
 * a. Load the configuration board size
 * b. Load the configuration snake and ladder
 * c. Load the configuration number of dice
 * d. Load the number of players who will play the game and all players should be at starting point location
 */


public class SnakeLadder {

    public static void main(String[] args) {

        Dice dice = new Dice(1);
        Player ranjeet = new Player(101, "Ranjeet");
        Player ankita = new Player(102, "Ankita");
        Queue<Player> players = new LinkedList<>();
        players.offer(ranjeet);
        players.offer(ankita);

        Jumper snake1 = new Jumper(10, 2);
        Jumper snake2 = new Jumper(99, 12);
        List<Jumper> snakes = new ArrayList<>();
        snakes.add(snake1);
        snakes.add(snake2);

        Jumper ladder1 = new Jumper(5, 25);
        Jumper ladder2 = new Jumper(40, 89);
        List<Jumper> ladders = new ArrayList<>();
        ladders.add(ladder1);
        ladders.add(ladder2);

        Map<String, Integer> playerPosition = new HashMap<>();
        playerPosition.put("Ranjeet", 0);
        playerPosition.put("Ankita", 0);


        GameBoard game = new GameBoard(dice, players, snakes, ladders, playerPosition, 100);

        game.startGame();


    }
}
