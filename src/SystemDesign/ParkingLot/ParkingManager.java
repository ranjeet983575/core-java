package SystemDesign.ParkingLot;

public class ParkingManager {
    public static void main(String[] args) {
        // Create parking lot
        ParkingLot parkingLot = new ParkingLot("PL1");

        // Create floors with different numbers of spots
        ParkingFloor floor1 = new ParkingFloor("F1", 10, 10, 10);
        ParkingFloor floor2 = new ParkingFloor("F2", 10, 10, 10);
        

        // Add floors to parking lot
        parkingLot.addFloor(floor1);
        parkingLot.addFloor(floor2);

        // Create vehicles
        Vehicle motorcycle = new Motorcycle("M1");
        Vehicle car = new Car("C1");
        Vehicle truck = new Truck("T1");

        // Park vehicles
        parkingLot.parkVehicle(motorcycle);  // Should park in a small spot
        parkingLot.parkVehicle(car);         // Should park in a medium or large spot
        parkingLot.parkVehicle(truck);       // Should park in a large spot

        // Remove vehicle
        parkingLot.removeVehicle(car);       // Should remove the car
    }
}

