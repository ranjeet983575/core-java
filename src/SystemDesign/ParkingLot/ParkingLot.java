package SystemDesign.ParkingLot;

import java.util.ArrayList;
import java.util.List;

class ParkingLot {
    private final String parkingLotId;
    private List<ParkingFloor> floors;

    public ParkingLot(String parkingLotId) {
        this.parkingLotId = parkingLotId;
        this.floors = new ArrayList<>();
    }

    public void addFloor(ParkingFloor floor) {
        floors.add(floor);
    }

    public ParkingSpot findSpotForVehicle(Vehicle vehicle) {
        for (ParkingFloor floor : floors) {
            ParkingSpot spot = floor.findAvailableSpot(vehicle);
            if (spot != null) {
                return spot;
            }
        }
        return null;
    }

    public void parkVehicle(Vehicle vehicle) {
        ParkingSpot spot = findSpotForVehicle(vehicle);
        if (spot != null) {
            spot.parkVehicle(vehicle);
            System.out.println("Vehicle parked at spot: " + spot.getSpotId());
        } else {
            System.out.println("No available spot for vehicle.");
        }
    }

    public void removeVehicle(Vehicle vehicle) {
        for (ParkingFloor floor : floors) {
            for (ParkingSpot spot : floor.getSpots().values()) {
                if (!spot.isFree() && spot.getVehicle().equals(vehicle)) {
                    spot.removeVehicle();
                    System.out.println("Vehicle removed from spot: " + spot.getSpotId());
                    return;
                }
            }
        }
        System.out.println("Vehicle not found.");
    }
}
