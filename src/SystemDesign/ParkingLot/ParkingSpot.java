package SystemDesign.ParkingLot;

class ParkingSpot {
    private final String spotId;
    private final ParkingSpotType spotType;
    private Vehicle vehicle;

    public ParkingSpot(String spotId, ParkingSpotType spotType) {
        this.spotId = spotId;
        this.spotType = spotType;
    }

    public boolean canFitVehicle(Vehicle vehicle) {
        switch (vehicle.getType()) {
            case MOTORCYCLE:
                return true;
            case CAR:
                return spotType == ParkingSpotType.MEDIUM || spotType == ParkingSpotType.LARGE;
            case TRUCK:
                return spotType == ParkingSpotType.LARGE;
            default:
                return false;
        }
    }

    public boolean isFree() {
        return vehicle == null;
    }

    public void parkVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public void removeVehicle() {
        this.vehicle = null;
    }

    public String getSpotId() {
        return spotId;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public ParkingSpotType getSpotType() {
        return spotType;
    }
}
