package SystemDesign.ParkingLot;

import java.util.HashMap;
import java.util.Map;

public class ParkingFloor {
    private final String floorId;
    private Map<String, ParkingSpot> spots;

    public ParkingFloor(String floorId, int smallSpots, int mediumSpots, int largeSpots) {
        this.floorId = floorId;
        this.spots = new HashMap<>();

        for (int i = 0; i < smallSpots; i++) {
            spots.put("S" + i, new ParkingSpot("S" + i, ParkingSpotType.SMALL));
        }
        for (int i = 0; i < mediumSpots; i++) {
            spots.put("M" + i, new ParkingSpot("M" + i, ParkingSpotType.MEDIUM));
        }
        for (int i = 0; i < largeSpots; i++) {
            spots.put("L" + i, new ParkingSpot("L" + i, ParkingSpotType.LARGE));
        }
    }

    public ParkingSpot findAvailableSpot(Vehicle vehicle) {
        for (ParkingSpot spot : spots.values()) {
            if (spot.isFree() && spot.canFitVehicle(vehicle)) {
                return spot;
            }
        }
        return null;
    }

    public String getFloorId() {
        return floorId;
    }

    public Map<String, ParkingSpot> getSpots() {
        return spots;
    }

    public void setSpots(Map<String, ParkingSpot> spots) {
        this.spots = spots;
    }
}
