package Recursion;

public class PrintOneToTen {

    public static void main(String[] args) {
        printNumberToOne(10);
        System.out.println();
        printOneToTen(1);
        System.out.println();
        System.out.println("POST");
        printNumberToOnePost(10);
    }

    public static void printNumberToOne(int num) {
        if (num == 1) {
            System.out.print(num + " ");
        } else {
            System.out.print(num + " ");
            printNumberToOne(num - 1);
        }
    }

    public static void printNumberToOnePost(int num) {
        if (num == 1) {
            System.out.print(num + "\t");
        } else {
            printNumberToOnePost(num - 1);
            System.out.print(num + "\t");
        }
    }

    public static void printOneToTen(int num) {
        if (num == 10) {
            System.out.print(num + " ");
        } else {
            System.out.print(num + " ");
            printOneToTen(num + 1);
        }
    }


}
