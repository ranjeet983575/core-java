package Recursion;

public class SumRecursive {
    public static void main(String[] args) {
        System.out.println("Sum : " + sumNum(5));
    }

    public static int sumNum(int num) {
        if (num == 1) {
            return 1;
        } else {
            return num + sumNum(num - 1);
        }
    }
}


