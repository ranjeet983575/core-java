package Recursion;

public class StringReverse {
    public static void main(String[] args) {
        String str = "Ranjeet";
        System.out.println(reverseString(str, str.length()));
        System.out.println(reverseString2(new StringBuilder(), str, str.length() - 1));
    }

    public static String reverseString(String str, int len) {
        if (len == 0) {
            return "";
        } else {
            return str.charAt(len - 1) + reverseString(str, len - 1);
        }
    }

    public static StringBuilder reverseString2(StringBuilder sb, String str, int len) {
        if (len < 0) {
            return sb;
        } else {
            sb.append(str.charAt(len));
            return reverseString2(sb, str, len - 1);
        }
    }
}
