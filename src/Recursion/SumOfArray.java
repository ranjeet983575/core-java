package Recursion;

public class SumOfArray {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5};
        System.out.println("Sum of Array is : " + sum(array, array.length));
    }

    public static int sum(int[] array, int len) {
        if (len == 0) {
            return 0;
        } else {
            return array[len - 1] + sum(array, len - 1);
        }
    }
}
